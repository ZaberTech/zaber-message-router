module.exports = {
  "env": {
    "es6": true,
    "node": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaVersion": 2018
  },
  "rules": {
    'indent': [
      'error', 
      2, 
      { 'SwitchCase': 1 }
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "camelcase": [
      "error"
    ],
    "prefer-const": [
      "error"
    ],
    "object-curly-spacing": [
      "error",
      "always"
    ],
    'brace-style': [
      'error',
      '1tbs',
      { 'allowSingleLine': true },
    ],
    "arrow-parens": [
      "error",
      "as-needed",
    ],
    "prefer-arrow-callback": [
      "error"
    ],
    "no-cond-assign": [
      "error",
      "except-parens"
    ],
  }
};