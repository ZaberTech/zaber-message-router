# Release

- Create release branch `release-X.X.X`
- Update `CHANGELOG.md`
- Update version in `package.json` and `internal\app\version.go`
- Run CI and execute manual
- Merge release branch and tag the commit as `vX.X.X.`
