
const path = require('path');
const { generateGoBinLicense } = require('@zaber/tools');

const forkReplacements = {
  'github.com/zabertech/go-serial': 'github.com/bugst/go-serial',
  'github.com/zabertech/go-ethereum': 'github.com/ethereum/go-ethereum',
};

async function collectLicenses() {
  const licensePath = path.join(__dirname, 'LICENSE.txt');
  await generateGoBinLicense(path.join(__dirname, '..'), licensePath, forkReplacements);
}

module.exports = { collectLicenses };
