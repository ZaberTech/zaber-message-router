import { expect } from 'chai';

import { writeYaml } from './yml';
import { CONFIG_FILE, itLinux64, tryConnect } from './utils';
import { spawn, RouterProcess } from './run';
import { TcpClient } from './tcp';
import { LAUNCHER_PIPE } from './constants';
import { RpcClient } from './rpc_client';
import { Tty } from './tty';
import { Tty0Tty, createTty0Tty } from './tty0tty';

describe('Serial port', () => {
  const client = new TcpClient();
  let rpcClient: RpcClient;

  let tty: Tty;
  let tty0tty: Tty0Tty;

  let routerProcess: RouterProcess;

  beforeEach(async () => {
    tty0tty = await createTty0Tty();
    tty = new Tty(tty0tty.tty2);
    await tty.open();

    await writeYaml(CONFIG_FILE, {
      connections: [{
        type: 'serial',
        serial_port: tty0tty.tty1,
        baud_rate: 115200,
      }],
    });

    routerProcess = spawn('run');

    rpcClient = new RpcClient(client);
    await tryConnect(client, { path: LAUNCHER_PIPE });

    await rpcClient.warmupConnection(tty0tty.tty1);
  });
  afterEach(async () => {
    await client.close();

    await routerProcess.end();

    await tty.close();
    await tty0tty.end();
  });

  itLinux64('communicates both ways', async () => {
    await rpcClient.sendMessage(tty0tty.tty1, '/');
    const message = await tty.popLine();
    expect(message).to.eq('/');

    const replyPromise = rpcClient.waitForReply(tty0tty.tty1);
    await tty.pushLine('@1 0 OK IDLE -- 0');
    expect(await replyPromise).to.eq('@1 0 OK IDLE -- 0');
  });
});
