import { expect } from 'chai';
import { skip, take, toArray } from 'rxjs/operators';
import _ from 'lodash';

import { writeYaml } from './yml';
import { CONFIG_FILE, tryConnect, cleanConfig, itCI } from './utils';
import { spawn, RouterProcess } from './run';
import { TcpServer, TEST_PORT } from './tcp_server';
import { TcpClient } from './tcp';
import { LAUNCHER_PIPE } from './constants';
import { RpcClient } from './rpc_client';

const CONNECTION_ID = `localhost:${TEST_PORT}`;

describe('Connections', () => {
  const server = new TcpServer();
  const client = new TcpClient();
  let rpcClient: RpcClient;

  before(async () => {
    await server.start();
  });
  after(async () => {
    await server.stop();
    await cleanConfig();
  });

  describe('single connection', () => {
    let routerProcess: RouterProcess;

    beforeEach(async () => {
      await writeYaml(CONFIG_FILE, {
        connections: [{
          type: 'tcp/ip',
          hostname: 'localhost',
          tcp_port: TEST_PORT,
        }],
      });

      routerProcess = spawn('run');

      rpcClient = new RpcClient(client);
      await tryConnect(client, { path: LAUNCHER_PIPE });

      await rpcClient.setClientVersion('2.0');
      await rpcClient.warmupConnection(CONNECTION_ID);
    });
    afterEach(async () => {
      await client.close();

      await routerProcess.end();

      server.closeSocket();
    });

    it('communicates both ways', async () => {
      await rpcClient.sendMessage(CONNECTION_ID, '/');
      const message = await server.pop();
      expect(message).to.eq('/');

      const replyPromise = rpcClient.waitForReply(CONNECTION_ID);
      await server.push('@1 0 OK IDLE -- 0');
      expect(await replyPromise).to.eq('@1 0 OK IDLE -- 0');
    });

    it('deals with out of order delivery and duplicity', async () => {
      await Promise.all([
        rpcClient.sendMessage(CONNECTION_ID, '/1', 1),
        rpcClient.sendMessage(CONNECTION_ID, '/0', 0),
        rpcClient.sendMessage(CONNECTION_ID, '/0', 0),
        rpcClient.sendMessage(CONNECTION_ID, '/4', 4),
        rpcClient.sendMessage(CONNECTION_ID, '/2', 2),
        rpcClient.sendMessage(CONNECTION_ID, '/5', 5),
        rpcClient.sendMessage(CONNECTION_ID, '/2', 2),
        rpcClient.sendMessage(CONNECTION_ID, '/3', 3),
      ]);
      const messages = await Promise.all(_.range(6).map(() => server.pop()));
      expect(messages).to.deep.eq(['/0', '/1', '/2', '/3', '/4', '/5']);
    });

    it('provides raising message IDs', async () => {
      const replies = rpcClient.replies.pipe(take(4), toArray()).toPromise();
      await Promise.all(_.range(4).map(() => server.push('data')));
      expect((await replies).map(reply => reply.id)).to.deep.eq([0, 1, 2, 3]);
    });

    it('resets message IDs on warmup', async () => {
      await Promise.all(_.range(3).map(id => rpcClient.sendMessage(CONNECTION_ID, '/', id)));
      let messages = await Promise.all(_.range(3).map(() => server.pop()));
      expect(messages).to.deep.eq(['/', '/', '/']);

      let replyPromise = rpcClient.replies.pipe(skip(2), take(1)).toPromise();
      await Promise.all(_.range(3).map(() => server.push('data')));
      expect((await replyPromise).id).to.eq(2);

      await rpcClient.warmupConnection(CONNECTION_ID);

      await Promise.all(_.range(3).map(id => rpcClient.sendMessage(CONNECTION_ID, '/', id)));
      messages = await Promise.all(_.range(3).map(() => server.pop()));
      expect(messages).to.deep.eq(['/', '/', '/']);

      replyPromise = rpcClient.replies.pipe(take(1)).toPromise();
      await server.push('data');
      expect((await replyPromise).id).to.eq(0);
    });

    itCI('fails connection because of missing message', async function(): Promise<void> {
      this.timeout(12 * 1000);

      await Promise.all([
        rpcClient.sendMessage(CONNECTION_ID, '/0', 0),
        rpcClient.sendMessage(CONNECTION_ID, '/1', 1),
        rpcClient.sendMessage(CONNECTION_ID, '/3', 3),
        rpcClient.sendMessage(CONNECTION_ID, '/4', 4),
      ]);
      const errorPromise = rpcClient.errors.pipe(take(1)).toPromise();

      expect(await errorPromise).to.deep.eq({
        connection: CONNECTION_ID,
        error: 'Connection was interrupted by missing packet',
      });
      expect(await server.pop()).to.eq('/0');
      expect(await server.pop()).to.eq('/1');
    });

    it('closes connection after rpc client disconnects', async () => {
      await rpcClient.sendMessage(CONNECTION_ID, '/');
      const message = await server.pop();
      expect(message).to.eq('/');

      const closingPromise = server.waitForClose();
      await rpcClient.cooldownConnection(CONNECTION_ID);
      await closingPromise;
    });

    it('throws an exception when trying to warmup an invalid connection', async () => {
      const promise = rpcClient.warmupConnection('invalid_connection');
      await expect(promise).to.be.rejectedWith('Connection with ID invalid_connection does not exist');
    });

    it('reconnects on a message', async () => {
      await rpcClient.sendMessage(CONNECTION_ID, '/');
      expect(await server.pop()).to.eq('/');

      const waitForError = rpcClient.errors.pipe(take(1)).toPromise();
      server.closeSocket();
      await waitForError;

      await rpcClient.sendMessage(CONNECTION_ID, '/');
      expect(await server.pop()).to.eq('/');
    });

    it('is listed in connections', async () => {
      const connections = await rpcClient.listConnections();
      expect(connections).deep.eq([{
        type: 'tcp/ip',
        id: CONNECTION_ID,
        hostname: 'localhost',
        tcp_port: 22789,
        name: '',
      }]);
    });

    it('sends error when the connection is removed', async () => {
      const errorPromise = rpcClient.errors.pipe(take(1)).toPromise();

      await rpcClient.removeConnection(CONNECTION_ID);

      expect(await errorPromise).to.deep.eq({
        connection: CONNECTION_ID,
        error: 'Connection stopped',
      });
    });

    it('sends error when the connection disconnects', async () => {
      await rpcClient.sendMessage(CONNECTION_ID, '/');
      expect(await server.pop()).to.eq('/');

      const errorPromise = rpcClient.errors.pipe(take(1)).toPromise();

      await server.closeSocket();

      expect(await errorPromise).to.deep.eq({
        connection: CONNECTION_ID,
        error: 'Cannot read from connection: EOF',
      });
    });

    it('sends error when the connection is not warmed up', async () => {
      await rpcClient.cooldownConnection(CONNECTION_ID);

      const errorPromise = rpcClient.errors.pipe(take(1)).toPromise();
      await rpcClient.sendMessage(CONNECTION_ID, '/');

      expect(await errorPromise).to.deep.eq({
        connection: CONNECTION_ID,
        error: 'Connection not ready: Unexpected write',
      });
    });
  });
});
