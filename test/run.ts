import childProcess, { ChildProcess } from 'child_process';
import path from 'path';
import os from 'os';

type ClosePromise = Promise<{ stdout: string; stderr: string; }>;

export class RouterProcess {
  constructor(
    private readonly process: ChildProcess,
    private readonly endPromise: ClosePromise,
    ) { }

    public end(): ClosePromise {
      this.process.kill();

      return this.waitTillEnd();
    }

    public waitTillEnd(): ClosePromise {
      return this.endPromise;
    }
}

const archToDir: {
  [key: string]: string
} = {
  x64: 'amd64',
  ia32: '386',
  arm: 'arm',
  arm64: 'arm64',
};
const platformToDir: {
  [key: string]: string
} = {
  win32: 'windows',
  linux: 'linux',
  darwin: 'darwin',
};

export function spawn(args: string): RouterProcess {
  let file = path.join(__dirname, '..', 'bin', platformToDir[os.platform()], archToDir[os.arch()], 'zaber-message-router');
  if (os.platform() === 'win32') {
    file += '.exe';
  }

  let newProcess: ChildProcess;
  const promise: ClosePromise = new Promise((resolve, reject) => {
    newProcess = childProcess.execFile(file, args.split(' '), {
      cwd: __dirname,
      env: {
        ...process.env,
        ZABER_MESSAGE_ROUTER_CONFIG_FILE: path.join(__dirname, 'message-router-config.yml'),
      }
    }, (error: any, stdout, stderr) => {
      const wasTerminated = error && error.killed === true && error!.signal === 'SIGTERM';
      if (error && !wasTerminated) {
        return reject({ error, stdout, stderr });
      }
      resolve({ stdout, stderr });
    });
  });

  return new RouterProcess(newProcess, promise);
}

export function run(args: string): ClosePromise {
  const instance = spawn(args);
  return instance.waitTillEnd();
}
