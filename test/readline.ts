import readline from 'readline';
import { Readable } from 'stream';

export class Readline {
  private readonly readline: readline.ReadLine;
  private lines: string[] = [];
  private waitLineQueue: Array<() => void> = [];
  private closed = false;

  constructor(input: Readable) {
    this.readline = readline.createInterface({ input });

    this.readline.on('line', line => {
      this.lines.push(line);

      const waitCb = this.waitLineQueue.shift();
      if (waitCb) {
        waitCb();
      }
    });
  }

  public async popLine(): Promise<string> {
    if (this.lines.length === 0 || this.waitLineQueue.length > 0) {
      if (this.closed) {
        throw new Error('Readline already closed');
      }

      await new Promise<void>(r => this.waitLineQueue.push(r));
    }

    return this.lines.shift() || null;
  }

  public close(): void {
    if (this.closed) {
      return;
    }
    this.closed = true;

    this.readline.close();

    while (this.waitLineQueue.length > 0) {
      this.waitLineQueue.shift()!();
    }
  }
}
