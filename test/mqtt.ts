import mqtt from 'mqtt';
import Bluebird from 'bluebird';
import got from 'got';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

import { API_URL, UNAUTHENTICATED_REALM } from './constants';

interface Message {
  topic: string;
  payload: string;
}

export class Mqtt {
  private client: mqtt.Client;
  private messages = new Subject<Message>();

  public async connect(token: string): Promise<void> {
    const { body: { url } } = await got(`${API_URL}/iot-token/websocket-url?token=${token}`, {
      responseType: 'json',
    });

    this.client = mqtt.connect(url, {
      clientId: `${UNAUTHENTICATED_REALM}-client-${Math.random()}`,
      clean: true,
      reconnectPeriod: -1,
    });

    this.client.on('message',
      (topic, payload) => this.messages.next({ topic, payload: payload.toString('utf8') })
    );

    await new Promise((resolve, reject) => {
      this.client.once('connect', resolve);
      this.client.once('error', reject);
    });
  }

  public disconnect(): void {
    if (!this.client) {
      return;
    }
    this.client.end(true);
    this.client = null;

    this.messages.complete();
    this.messages = new Subject<Message>();
  }

  public async subscribe(topic: string): Promise<Observable<Message>> {
    const matchingRegexp = new RegExp(`^${topic}$`.replace('/', '\\/').replace('+', '\\w+').replace('#$', ''));
    await Bluebird.fromNode(cb => this.client.subscribe(topic, { qos: 0 }, cb));
    return this.messages.pipe(filter(message => matchingRegexp.test(message.topic)));
  }

  public async publish(topic: string, message: string): Promise<void> {
    await Bluebird.fromNode(cb => this.client.publish(topic, message, { qos: 0 }, cb));
  }
}
