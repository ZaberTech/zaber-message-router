import { expect } from 'chai';
import _ from 'lodash';
import Bluebird from 'bluebird';

import { writeYaml } from './yml';
import { CONFIG_FILE, tryConnect, cleanConfig, getID } from './utils';
import { spawn, RouterProcess } from './run';
import { TcpServer, TEST_PORT } from './tcp_server';
import { TcpClient } from './tcp';
import { LAUNCHER_PIPE } from './constants';
import { RpcClient } from './rpc_client';

const CONNECTION_ID = `localhost:${TEST_PORT}`;

describe('Connections', () => {
  const server = new TcpServer();
  const client1 = new TcpClient();
  const client2 = new TcpClient();
  let rpcClient1: RpcClient;
  let rpcClient2: RpcClient;

  before(async () => {
    await server.start();
  });
  after(async () => {
    await server.stop();
    await cleanConfig();
  });

  describe('ID mapping', () => {
    let routerProcess: RouterProcess;

    beforeEach(async () => {
      await writeYaml(CONFIG_FILE, {
        connections: [{
          type: 'tcp/ip',
          hostname: 'localhost',
          tcp_port: TEST_PORT,
        }],
      });

      routerProcess = spawn('run');

      rpcClient1 = new RpcClient(client1);
      await tryConnect(client1, { path: LAUNCHER_PIPE });
      rpcClient2 = new RpcClient(client2);
      await tryConnect(client2, { path: LAUNCHER_PIPE });

      await rpcClient1.warmupConnection(CONNECTION_ID);
      await rpcClient2.warmupConnection(CONNECTION_ID);
    });
    afterEach(async () => {
      await client1.close();
      await client2.close();

      if (routerProcess) {
        await routerProcess.end();
      }

      server.closeSocket();
    });

    it('avoids ID collision from two connections', async () => {
      await rpcClient1.sendMessage(CONNECTION_ID, '/0 0 0');
      await rpcClient2.sendMessage(CONNECTION_ID, '/0 0 0');

      const messages = await Promise.all([server.pop(), server.pop()]);
      const ids = messages.map(message => getID(message));
      expect(ids[0]).not.to.eq(ids[1]);

      const replyPromise = Promise.all([
        rpcClient1.waitForReply(CONNECTION_ID),
        rpcClient2.waitForReply(CONNECTION_ID),
      ]);
      await server.push(`@1 0 ${ids[1]} OK IDLE -- 0`);
      await server.push(`@1 0 ${ids[0]} OK IDLE -- 0`);

      expect(await replyPromise).to.deep.eq([
        '@1 0 0 OK IDLE -- 0',
        '@1 0 0 OK IDLE -- 0',
      ]);
    });

    it('maps the ID to the same ID when clients sends two messages with same ID', async () => {
      await rpcClient1.sendMessage(CONNECTION_ID, '/1 0 33 get all');
      const request = await server.pop();
      const id = getID(request);

      await rpcClient1.sendMessage(CONNECTION_ID, '/1 0 34');
      const request2 = await server.pop();
      const id2 = getID(request2);

      await rpcClient1.sendMessage(CONNECTION_ID, '/1 0 33');
      const request3 = await server.pop();
      const id3 = getID(request3);

      expect(id3).to.eq(id);
      expect(id2).to.not.eq(id);
    });

    it('maps all returning responses to the same ID', async () => {
      const client2Replies = [];
      rpcClient2.replies.subscribe(reply => client2Replies.push(reply));

      await rpcClient1.sendMessage(CONNECTION_ID, '/1 0 33 get all');
      const request = await server.pop();
      const id = getID(request);

      const reply1Promise = rpcClient1.waitForReply(CONNECTION_ID);
      await server.push(`@1 0 ${id} OK IDLE -- 0`);
      const reply1 = await reply1Promise;

      const reply2Promise = rpcClient1.waitForReply(CONNECTION_ID);
      await server.push(`#1 0 ${id} OK IDLE -- setting bla bla`);
      const reply2 = await reply2Promise;

      expect([reply1, reply2]).to.deep.eq([
        '@1 0 33 OK IDLE -- 0',
        '#1 0 33 OK IDLE -- setting bla bla',
      ]);
      expect(client2Replies.length).to.eq(0);
    });

    it('allocates IDs according to whether there was already reply to them', async () => {
      const usedIDs = new Set<number>();
      for (let id = 0; id <= 97; id++) {
        await rpcClient1.sendMessage(CONNECTION_ID, `/0 0 ${id}`);
        const request = await server.pop();
        const mappedID = getID(request);
        usedIDs.add(mappedID);
      }
      expect(usedIDs.size).to.eq(98);

      for (let id = 0; id < 10; id++) {
        await rpcClient2.sendMessage(CONNECTION_ID, `/0 0 ${id}`);
        const request = await server.pop();
        const mappedID = getID(request);

        expect(usedIDs.has(mappedID)).to.eq(false);

        const replyPromise = rpcClient2.waitForReply(CONNECTION_ID);
        await server.push(`@1 0 ${mappedID} OK IDLE -- 0`);
        const reply = await replyPromise;

        expect(reply).to.eq(`@1 0 ${id} OK IDLE -- 0`);
      }
    });

    it('recycles IDs equally', async () => {
      const ITERATIONS = 3;
      const idUsages = _.range(0, 100).map(() => 0);

      for (let iterations = 0; iterations < ITERATIONS; iterations++) {
        for (let id = 0; id <= 99; id++) {
          await rpcClient1.sendMessage(CONNECTION_ID, `/0 0 ${id}`);
          const request = await server.pop();
          const mappedID = getID(request);
          idUsages[mappedID]++;
        }
      }

      expect(idUsages.every(usage => usage === ITERATIONS)).to.eq(true);
    });

    it('preserves LRC in messages recomputing it', async () => {
      for (let id = 0; id < 5; id++) {
        await rpcClient1.sendMessage(CONNECTION_ID, addLRC(`/0 0 ${id} get pos`));

        const request = await server.pop(true);
        expect(request).to.eq(addLRC(request));

        const mappedID = getID(request);

        const replyPromise = rpcClient1.waitForReply(CONNECTION_ID);
        await server.push(addLRC(`@1 0 ${mappedID} OK IDLE -- 0`));
        const reply = await replyPromise;

        expect(reply).to.eq(addLRC(`@1 0 ${id} OK IDLE -- 0`));
      }
    });

    it('broadcasts alerts and everything else', async () => {
      let replyPromise = Promise.all([
        rpcClient1.waitForReply(CONNECTION_ID),
        rpcClient2.waitForReply(CONNECTION_ID),
      ]);
      await server.push('!02 1 IDLE -- 0');
      expect(await replyPromise).to.deep.eq([
        '!02 1 IDLE -- 0',
        '!02 1 IDLE -- 0',
      ]);

      replyPromise = Promise.all([
        rpcClient1.waitForReply(CONNECTION_ID),
        rpcClient2.waitForReply(CONNECTION_ID),
      ]);
      await server.push('bla bla');
      expect(await replyPromise).to.deep.eq([
        'bla bla',
        'bla bla',
      ]);
    });

    it('does not broadcast replies with unknown IDs', async () => {
      await rpcClient1.sendMessage(CONNECTION_ID, '/0 0 31');
      await rpcClient2.sendMessage(CONNECTION_ID, '/0 0 41');
      const id1 = getID(await server.pop());
      const id2 = getID(await server.pop());
      const unknownId = _.difference([1, 2, 3], [id1, id2]).pop(); // thx Paul

      const replyPromise = Promise.all([
        rpcClient1.waitForReply(CONNECTION_ID),
        rpcClient2.waitForReply(CONNECTION_ID),
      ]);

      await server.push(`@1 0 ${unknownId} OK IDLE -- 0`);
      await server.push(`@1 0 ${id1} OK IDLE -- 0`);
      await server.push(`@1 0 ${id2} OK IDLE -- 0`);

      expect(await replyPromise).to.deep.eq([
        '@1 0 31 OK IDLE -- 0',
        '@1 0 41 OK IDLE -- 0',
      ]);
    });

    it('does not broadcast replies for disconnected clients', async () => {
      await rpcClient1.sendMessage(CONNECTION_ID, '/0 0 31');
      await rpcClient2.sendMessage(CONNECTION_ID, '/0 0 41');
      const id1 = getID(await server.pop());
      const id2 = getID(await server.pop());

      await client1.close();
      await Bluebird.delay(1000);

      const replyPromise = rpcClient2.waitForReply(CONNECTION_ID);

      // multiple message for client1 to detect the pipe is closed
      for (let i = 0; i < 3; i++) {
        await server.push(`@1 0 ${id1} OK IDLE -- 0`);
      }

      await server.push(`@1 0 ${id2} OK IDLE -- 0`);
      expect(await replyPromise).to.eq('@1 0 41 OK IDLE -- 0');
    });

    it('queues up all the command continuations until the last packet', async () => {
      await rpcClient1.sendMessage(CONNECTION_ID, '/0 0 0 get\\');

      await rpcClient2.sendMessage(CONNECTION_ID, '/0 0 0 get motor.current.max');
      const cmd2 = await server.pop();
      const id2 = getID(cmd2);
      expect(cmd2).to.equal(`/0 0 ${id2} get motor.current.max`);
      await server.push(`@1 0 ${id2} OK IDLE -- 100`);

      await rpcClient1.sendMessage(CONNECTION_ID, '/0 0 1 get motor.current.min');
      const cmd1 = await server.pop();
      const id1 = getID(cmd1);
      expect(cmd1).to.equal(`/0 0 ${id1} get motor.current.min`);
      await server.push(`@1 0 ${id1} OK IDLE -- 1`);

      await rpcClient1.sendMessage(CONNECTION_ID, '/0 0 0 cont 1 resolution');
      const cmd1start = await server.pop();
      const id1cont = getID(cmd1start);
      expect(cmd1start).to.equal(`/0 0 ${id1cont} get\\`);
      const cmd1cont = await server.pop();
      expect(cmd1cont).to.equal(`/0 0 ${id1cont} cont 1 resolution`);
      await server.push(`@1 0 ${id1cont} OK IDLE -- 10`);
    });
  });
});

function addLRC(message: string): string {
  const lrc = computeLRC(message).toString(16).padStart(2, '0').toUpperCase();
  return `${message.replace(/\:\w+$/, '')}:${lrc}`;
}

function computeLRC(message: string): number {
  let lrc = 0;
  for (let i = 0; i < message.length; i++) {
    if (message[i] === ':') {
      break;
    }
    lrc -= message.charCodeAt(i);
  }
  lrc = ((lrc % 256) + 256) % 256;
  return lrc;
}
