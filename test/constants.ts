import os from 'os';
import { execSync } from 'child_process';
import _ from 'lodash';

import folders from '@folder/xdg';

export const LAUNCHER_PIPE = (() => {
  if (os.platform() === 'win32') {
    const userInfo = execSync('C:\\WINDOWS\\system32\\whoami /user /fo csv /nh', { encoding: 'utf8' });
    const sid = _.chain(userInfo.split(',')).last().trim().trim('"').value();
    return `\\\\.\\pipe\\zaber-message-router-${sid}`;
  } else {
    return `${folders({ platform: process.platform }).runtime}/zaber-message-router-${os.userInfo().uid}.sock`;
  }
})();
export const API_URL = 'https://api-staging.zaber.io';
export const UNAUTHENTICATED_REALM = 'unauthenticated';
export const UNAUTHENTICATED_TOKEN = 'unauthenticated';
