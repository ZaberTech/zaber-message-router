import { createServer, Server, Socket } from 'net';
import readline from 'readline';
import _ from 'lodash';
import Bluebird from 'bluebird';

export const TEST_PORT = 22789;
export const TEST_HOST = '127.0.0.1';

export class TcpServer {
  private tcpListener?: Server;
  private socket: Socket | null = null;
  private readline: readline.ReadLine | null = null;
  private lines: string[] = [];
  private waitOnLine: Array<() => void> = [];
  private waitOnConnection: (() => void) | null = null;

  constructor() {
    this.onLine = this.onLine.bind(this);
    this.handleSocket = this.handleSocket.bind(this);
  }

  public async start(): Promise<void> {
    this.tcpListener = createServer(this.handleSocket);

    await Bluebird.fromCallback(cb => this.tcpListener!.listen({
      host: TEST_HOST,
      port: TEST_PORT,
    }, cb as any));
  }

  public async stop(): Promise<void> {
    const closePromise = Bluebird.fromCallback(cb => this.tcpListener!.close(cb));
    this.closeSocket();
    await closePromise;
  }

  public closeSocket(): void {
    if (this.lines.length > 0) {
      throw new Error(`Unprocessed lines in the buffer: ${this.lines.join()}.`);
    }
    if (!this.socket) {
      return;
    }
    this.socket.end();
    this.socket.destroy();
    this.socket = null;

    this.readline!.close();
    this.readline = null;

    this.lines = [];
    this.waitOnLine = [];
    this.waitOnConnection = null;
  }

  public async waitForConnection(): Promise<void> {
    if (!this.socket) {
      await new Promise<void>(resolve => this.waitOnConnection = resolve);
    }
  }

  public async waitForClose(): Promise<void> {
    await new Promise<void>(resolve => this.socket!.once('close', resolve));
  }

  public async pop(keepChecksum: boolean = false): Promise<string> {
    if (this.lines.length === 0) {
      await new Promise<void>(resolve => this.waitOnLine.push(resolve));
    }

    const [line] = this.lines.splice(0, 1);
    if (keepChecksum) {
      return line;
    } else {
      return line.replace(/\:.{2}$/, '');
    }
  }

  public async push(line: string): Promise<void> {
    await Bluebird.fromCallback(cb => this.socket!.write(line + '\n', cb));
  }

  private handleSocket(socket: Socket): void {
    this.closeSocket();
    this.socket = socket;

    this.socket.on('error', () => {
      if (this.socket === socket) {
        this.closeSocket();
      }
    });

    socket.setEncoding('utf8');
    this.readline = readline.createInterface({
      input: socket,
      output: socket,
    });
    this.readline.on('line', this.onLine);
    this.readline.on('error', () => null);

    const wait = this.waitOnConnection;
    if (wait) {
      this.waitOnConnection = null;
      wait();
    }
  }

  private onLine(line: string): void {
    this.lines.push(line);

    const [wait] = this.waitOnLine.splice(0, 1);
    if (wait) {
      wait();
    }
  }
}
