import net, { SocketConnectOpts } from 'net';
import Bluebird from 'bluebird';
import readline from 'readline';
import { Observable, Subject } from 'rxjs';

export class TcpClient {
  private readline: readline.ReadLine;
  private readonly socket = new net.Socket();
  private endPromise: Bluebird.Resolver<void>;
  private error: any = null;
  private _closed = true;
  private linesSubject = new Subject<string>();
  public get lines(): Observable<string> {
    return this.linesSubject;
  }

  public get closed(): boolean {
    return this._closed;
  }

  public async connect(path: SocketConnectOpts): Promise<void> {

    this.socket.once('close', hadErr => {
      this._closed = true;

      if (this.endPromise) {
        if (hadErr) {
          this.endPromise.reject(new Error('Transmission error'));
        } else {
          this.endPromise.resolve();
        }
      }

      if (this.readline) {
        this.readline.close();
        this.readline = null;
      }

      this.socket.removeAllListeners('error');
    });

    this.error = null;
    this.socket.once('error', err => this.error = err);

    await new Promise<void>((resolve, reject) => {
      this.socket.once('connect', () => {
        this._closed = false;
        resolve();
      });
      this.socket.once('error', reject);

      this.socket.connect(path);
    });

    this.readline = readline.createInterface({ input: this.socket });
    this.readline.on('line', line => this.linesSubject.next(line));
    this.readline.on('error', () => null); // caught by socket
  }

  public async pushLine(line: string): Promise<void> {
    if (this.error) {
      throw this.error;
    }
    await Bluebird.fromCallback(cb => this.socket.write(line + '\n', cb));
  }

  public async close(): Promise<void> {
    if (this._closed) {
      return;
    }

    await Promise.all([
      this.waitForClosing(),
      Bluebird.fromCallback(cb => this.socket.end(() => cb(null))),
    ]);

    this.linesSubject.complete();
    this.linesSubject = new Subject();
  }

  public async waitForClosing(): Promise<void> {
    if (!this.endPromise) {
      this.endPromise = Bluebird.defer();
    }
    try {
      await this.endPromise;
    } finally {
      this.endPromise = null;
    }
  }
}
