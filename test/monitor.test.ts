import { expect } from 'chai';
import { take, toArray, takeUntil, tap, mergeMap } from 'rxjs/operators';
import { from, timer } from 'rxjs';
import _ from 'lodash';

import { writeYaml } from './yml';
import { CONFIG_FILE, tryConnect, cleanConfig, getID } from './utils';
import { spawn, RouterProcess } from './run';
import { TcpServer, TEST_PORT } from './tcp_server';
import { TcpClient } from './tcp';
import { LAUNCHER_PIPE } from './constants';
import { MonitorMessage, RpcClient } from './rpc_client';
import { performance } from 'perf_hooks';
import { delay } from 'bluebird';

const CONNECTION_ID = `localhost:${TEST_PORT}`;

describe('Monitor', () => {
  const server = new TcpServer();

  before(async () => {
    await server.start();
  });
  after(async () => {
    await server.stop();
    await cleanConfig();
  });

  describe('single monitor, single client', () => {
    let routerProcess: RouterProcess;

    const tcpClient = new TcpClient();
    const tcpMonitorClient = new TcpClient();

    let client: RpcClient;
    let monitorClient: RpcClient;

    beforeEach(async () => {
      await writeYaml(CONFIG_FILE, {
        connections: [{
          type: 'tcp/ip',
          hostname: 'localhost',
          tcp_port: TEST_PORT,
        }],
      });

      routerProcess = spawn('run');

      client = new RpcClient(tcpClient);
      monitorClient = new RpcClient(tcpMonitorClient);

      await tryConnect(tcpClient, { path: LAUNCHER_PIPE });
      await tryConnect(tcpMonitorClient, { path: LAUNCHER_PIPE });

      await client.warmupConnection(CONNECTION_ID);
      await monitorClient.startMonitoring(CONNECTION_ID);
    });
    afterEach(async () => {
      await tcpClient.close();
      await tcpMonitorClient.close();

      await routerProcess.end();

      server.closeSocket();
    });

    it('monitors messages under a given connection ID', async () => {
      const bundlePromise1 = monitorClient.monitors.pipe(take(1)).toPromise();
      await client.sendMessage(CONNECTION_ID, '/');
      await server.pop();
      const bundle1 = await bundlePromise1;

      const bundlePromise2 = monitorClient.monitors.pipe(take(1)).toPromise();
      await server.push('@1 0 OK IDLE -- 0');
      const bundle2 = await bundlePromise2;

      expect(bundle1).to.deep.eq({
        id: 0,
        connection: 'localhost:22789',
        messages: [{
          l: '/',
          s: 'cmd-other',
        }],
      });
      expect(bundle2).to.deep.eq({
        id: 1,
        connection: 'localhost:22789',
        messages: [{
          l: '@1 0 OK IDLE -- 0',
          s: 'broadcast'
        }],
      });
    });

    it('stops sending messages when monitor client calls stop', async () => {
      const monitorPromise = monitorClient.monitors.pipe(take(1)).toPromise();
      await client.sendMessage(CONNECTION_ID, '/');
      await server.pop();

      const monitor = await monitorPromise;
      expect(monitor).to.deep.include({
        connection: 'localhost:22789',
      });

      await monitorClient.stopMonitoring(CONNECTION_ID);
      const testTimer = timer(1000);
      const emptyPromise = monitorClient.monitors.pipe(takeUntil(testTimer), toArray()).toPromise();

      await client.sendMessage(CONNECTION_ID, '/');
      await server.pop();

      const stoppedMonitor = await emptyPromise;
      expect(stoppedMonitor).to.have.lengthOf(0);
    });

    it('properly increments message IDs', async () => {
      const pushes: Array<Promise<void>> = [];
      const pushData = () => pushes.push(server.push('data'));

      const monitoredMessages = monitorClient.monitors.pipe(tap(pushData), take(4), toArray()).toPromise();
      pushData();

      expect((await monitoredMessages).map(bundle => bundle.id).sort()).to.deep.eq([0, 1, 2, 3]);
      await Promise.all(pushes);
    });

    it('resets message IDs when monitoring is started again', async () => {
      const promise1 = monitorClient.monitors.pipe(take(1)).toPromise();
      await client.sendMessage(CONNECTION_ID, '/home');
      await server.pop();
      const bundle1 = await promise1;

      await monitorClient.startMonitoring(CONNECTION_ID);

      const promise2 = monitorClient.monitors.pipe(take(1)).toPromise();
      await client.sendMessage(CONNECTION_ID, '/');
      await server.pop();
      const bundle2 = await promise2;

      expect(bundle1.id).to.eq(0);
      expect(bundle2.id).to.eq(0);
    });

    it('does nothing when stopMonitoring is called a second time', async () => {
      await monitorClient.stopMonitoring(CONNECTION_ID);
      await monitorClient.stopMonitoring(CONNECTION_ID);
    });

    it('marks messages according to their origin', async () => {
      await monitorClient.warmupConnection(CONNECTION_ID);

      const promise = monitorClient.monitors.pipe(
        mergeMap(bundle => from(bundle.messages)),
        take(5),
        toArray()
      ).toPromise();

      await client.sendMessage(CONNECTION_ID, '/1 0 0 move');
      const id1 = getID(await server.pop());
      await monitorClient.sendMessage(CONNECTION_ID, '/1 0 0 home');
      const id2 = getID(await server.pop());
      await server.push(`@1 0 ${id1} OK IDLE -- 10`);
      await server.push(`@1 0 ${id2} OK IDLE -- 20`);
      await server.push(`!alert`);

      const messages = await promise;
      messages.sort((a, b) => a.s.localeCompare(b.s));

      expect(messages).to.deep.eq([{
          l: '!alert',
          s: 'broadcast',
        }, {
          l: '/1 0 99 home',
          s: 'cmd',
        }, {
          l: '/1 0 0 move',
          s: 'cmd-other',
        }, {
          l: '@1 0 99 OK IDLE -- 20',
          s: 'reply',
        }, {
          l: '@1 0 0 OK IDLE -- 10',
          s: 'reply-other',
      }]);
    });

    it('dispatches first messages quickly after break but then prolongs the period', async () => {
      const durations: number[] = [];
      for (let i = 0; i < 4; i++) {
        if (i === 3) {
          await delay(500); // break between messages allows for burst to recharge
        }
        const promise1 = monitorClient.monitors.pipe(take(1)).toPromise();
        const start = performance.now();

        await client.sendMessage(CONNECTION_ID, '/home');
        await server.pop();
        await promise1;

        const duration = performance.now() - start;
        durations.push(duration);
      }
      const MIN_LONGER_WAIT_MS = 300;
      expect(durations[0] + MIN_LONGER_WAIT_MS).to.be.lessThan(durations[2]);
      expect(durations[1] + MIN_LONGER_WAIT_MS).to.be.lessThan(durations[2]);
      expect(durations[3] + MIN_LONGER_WAIT_MS).to.be.lessThan(durations[2]);
    });
  });

  describe('multiple clients, multiple monitor', () => {
    let routerProcess: RouterProcess;

    let tcpClientList: TcpClient[] = [];
    let tcpMonitorList: TcpClient[] = [];

    let clientList: RpcClient[] = [];
    let monitorList: RpcClient[] = [];

    beforeEach(async () => {
      await writeYaml(CONFIG_FILE, {
        connections: [{
          type: 'tcp/ip',
          hostname: 'localhost',
          tcp_port: TEST_PORT,
        }],
      });

      routerProcess = spawn('run');

      for (const i of _.range(10)) {
        tcpClientList.push(new TcpClient());
        clientList.push(new RpcClient(tcpClientList[i]));

        tcpMonitorList.push(new TcpClient());
        monitorList.push(new RpcClient(tcpMonitorList[i]));

        await tryConnect(tcpClientList[i], { path: LAUNCHER_PIPE });
        await clientList[i].warmupConnection(CONNECTION_ID);

        await tryConnect(tcpMonitorList[i], { path: LAUNCHER_PIPE });
        await monitorList[i].startMonitoring(CONNECTION_ID);
      }
    });

    afterEach(async () => {
      for (const i of _.range(10)) {
        await tcpClientList[i].close();
        await tcpMonitorList[i].close();
      }

      tcpClientList = [];
      tcpMonitorList = [];
      clientList = [];
      monitorList = [];

      await routerProcess.end();

      server.closeSocket();
    });

    it('monitors messages under a given connection ID', async () => {
      const monitorPromises: Array<Promise<MonitorMessage[]>> = [];
      const expected: MonitorMessage[] = [];

      for (const i of _.range(10)) {
        monitorPromises.push(monitorList[i].monitors.pipe(
          mergeMap(bundle => from(bundle.messages)),
          take(20),
          toArray()
        ).toPromise());
      }

      for (const i of _.range(10)) {
        await clientList[i].sendMessage(CONNECTION_ID, `/${i}`);
        await server.pop();

        const replyPromise = clientList[i].waitForReply(CONNECTION_ID);
        await server.push(`@1 0 OK IDLE -- ${i}`);
        await replyPromise;

        expected.push({
          s: 'cmd-other',
          l: `/${i}`,
        }, {
          s: 'broadcast',
          l: `@1 0 OK IDLE -- ${i}`,
        });
      }

      expected.sort((a, b) => a.l.localeCompare(b.l));

      for (const i of _.range(10)) {
        const messages = await monitorPromises[i];
        messages.sort((a, b) => a.l.localeCompare(b.l));
        expect(messages).to.deep.eq(expected);
      }
    });
  });
});
