import YAML from 'yaml';
import { promises as fs } from 'fs';

export async function readYaml(file: string): Promise<any> {
  const content = await fs.readFile(file, 'utf8');
  return YAML.parse(content);
}

export async function writeYaml(file: string, content: any): Promise<void> {
  await fs.writeFile(file, YAML.stringify(content), 'utf8');
}
