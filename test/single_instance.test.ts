import { expect } from 'chai';
import { run, spawn } from './run';

describe('single instance', () => {
  it('allows only single instance to run', async () => {
    const router1 = spawn('run');
    try {
      await run('run');
      throw new Error('No error');
    } catch (err) {
      expect(err.error?.code).to.eq(2);
    }
    await router1.end();
  });
});
