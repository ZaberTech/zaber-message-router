import { tryConnect } from './utils';
import { spawn, RouterProcess } from './run';
import { TcpClient } from './tcp';
import { LAUNCHER_PIPE } from './constants';
import { RpcClient } from './rpc_client';

describe('Control API', () => {
  const client = new TcpClient();
  let rpcClient: RpcClient;
  let routerProcess: RouterProcess;

  beforeEach(async () => {
    routerProcess = spawn('run');

    rpcClient = new RpcClient(client);
    await tryConnect(client, { path: LAUNCHER_PIPE });
  });

  afterEach(async () => {
    await client.close();
    await routerProcess.end();
  });

  it('exits', async () => {
    await rpcClient.controlExit();
    await routerProcess.waitTillEnd();
  });
});
