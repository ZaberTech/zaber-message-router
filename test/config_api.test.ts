import { expect } from 'chai';

import { tryConnect, cleanConfig, CONFIG_FILE, itLinux64, afterLinux64, beforeLinux64 } from './utils';
import { spawn, RouterProcess } from './run';
import { TcpServer, TEST_PORT } from './tcp_server';
import { TcpClient } from './tcp';
import { LAUNCHER_PIPE } from './constants';
import { RpcClient } from './rpc_client';
import { writeYaml, readYaml } from './yml';
import { createTty0Tty, Tty0Tty } from './tty0tty';
import { Tty } from './tty';

describe('Config API', () => {
  const client = new TcpClient();
  let rpcClient: RpcClient;
  let routerProcess: RouterProcess;

  beforeEach(async () => {
    await cleanConfig();
  });
  afterEach(async () => {
    await cleanConfig();
  });

  describe('TCP/IP', () => {
    const server = new TcpServer();

    before(async () => {
      await server.start();
    });
    after(async () => {
      await server.stop();
    });

    describe('add connection', () => {
      let connectionInfo: any;

      beforeEach(async () => {
        routerProcess = spawn('run');

        rpcClient = new RpcClient(client);
        await tryConnect(client, { path: LAUNCHER_PIPE });

        connectionInfo = await rpcClient.addTcpIp({ hostname: 'localhost', tcp_port: TEST_PORT });
      });

      afterEach(async () => {
        await client.close();

        await routerProcess.end();

        server.closeSocket();
      });

      it('returns correct info', async () => {
        expect(connectionInfo).to.deep.eq({
          type: 'tcp/ip',
          id: `localhost:${TEST_PORT}`,
          hostname: 'localhost',
          tcp_port: 22789,
          name: '',
        });
      });

      it('allows connection', async () => {
        await rpcClient.warmupConnection(connectionInfo.id);

        await rpcClient.sendMessage(connectionInfo.id, '/');
        expect(await server.pop()).to.eq('/');
      });

      it('is contained in connection list', async () => {
        const infos = await rpcClient.listConnections();
        expect(infos).to.deep.eq([connectionInfo]);
      });

      it('adds connection to the config file', async () => {
        const config = await readYaml(CONFIG_FILE);
        expect(config.connections).to.deep.eq([{
          type: 'tcp/ip',
          hostname: 'localhost',
          tcp_port: 22789,
          name: '',
        }]);
      });

      it('does not allow to add the same combination of hostname and port again', async () => {
        const promise = rpcClient.addTcpIp({ hostname: 'localhost', tcp_port: TEST_PORT });
        await expect(promise).to.be.rejectedWith('Hostname and port combination localhost:22789 is used in the config more than once');
      });

      it('sets connection name', async () => {
        await rpcClient.setConnectionName(`localhost:${TEST_PORT}`, 'Controller 1');
        const config = await readYaml(CONFIG_FILE);
        expect(config.connections).to.deep.eq([{
          type: 'tcp/ip',
          hostname: 'localhost',
          tcp_port: TEST_PORT,
          name: 'Controller 1',
        }]);
      });
    });

    describe('remove connection', () => {
      const CONNECTION_ID =  `localhost:${TEST_PORT}`;

      beforeEach(async () => {
        await writeYaml(CONFIG_FILE, {
          connections: [{
            type: 'tcp/ip',
            hostname: 'localhost',
            tcp_port: TEST_PORT,
          }],
        });

        routerProcess = spawn('run');

        rpcClient = new RpcClient(client);
        await tryConnect(client, { path: LAUNCHER_PIPE });
      });

      afterEach(async () => {
        await client.close();

        await routerProcess.end();

        server.closeSocket();
      });

      it('removes connection from list', async () => {
        await rpcClient.removeConnection(CONNECTION_ID);

        const infos = await rpcClient.listConnections();
        expect(infos).to.deep.eq([]);
      });

      it('removes connection from the config file', async () => {
        await rpcClient.removeConnection(CONNECTION_ID);

        const config = await readYaml(CONFIG_FILE);
        expect(config.connections).to.deep.eq([]);
      });

      it('stops the connection', async () => {
        await rpcClient.warmupConnection(CONNECTION_ID);
        await rpcClient.sendMessage(CONNECTION_ID, '/');
        expect(await server.pop()).to.eq('/');

        await rpcClient.removeConnection(CONNECTION_ID);

        const promise = rpcClient.warmupConnection(CONNECTION_ID);
        await expect(promise).to.be.rejectedWith(`Connection with ID ${CONNECTION_ID} does not exist`);
      });
    });
  });

  describe('Serial Port', () => {
    let tty: Tty;
    let tty0tty: Tty0Tty;

    beforeLinux64(async () => {
      tty0tty = await createTty0Tty();
      tty = new Tty(tty0tty.tty2);
      await tty.open();
    });
    afterLinux64(async () => {
      await tty.close();
      await tty0tty.end();
    });

    describe('add connection', () => {
      let connectionInfo: any;

      beforeEach(async () => {
        routerProcess = spawn('run');

        rpcClient = new RpcClient(client);
        await tryConnect(client, { path: LAUNCHER_PIPE });

        connectionInfo = await rpcClient.addSerialPort({ serial_port: tty0tty.tty1 });
      });

      afterEach(async () => {
        await client.close();

        await routerProcess.end();
      });

      itLinux64('returns correct info', async () => {
        expect(connectionInfo).to.deep.eq({
          type: 'serial',
          id: tty0tty.tty1,
          serial_port: tty0tty.tty1,
          baud_rate: 115200,
          name: '',
        });
      });

      itLinux64('allows connection', async () => {
        await rpcClient.warmupConnection(connectionInfo.id);

        await rpcClient.sendMessage(connectionInfo.id, '/');
        expect(await tty.popLine()).to.eq('/');
      });

      itLinux64('is contained in connection list', async () => {
        const infos = await rpcClient.listConnections();
        expect(infos).to.deep.eq([connectionInfo]);
      });

      itLinux64('adds connection to the config file', async () => {
        const config = await readYaml(CONFIG_FILE);
        expect(config.connections).to.deep.eq([{
          type: 'serial',
          serial_port: tty0tty.tty1,
          baud_rate: 115200,
          name: '',
        }]);
      });

      itLinux64('does not allow to add the same serial port again', async () => {
        const promise = rpcClient.addSerialPort({ serial_port: tty0tty.tty1 });
        await expect(promise).to.be.rejectedWith(`Serial port ${tty0tty.tty1} is used in the config more than once`);
      });
    });
  });

  describe('multiple connections', () => {
    beforeEach(async () => {
      await writeYaml(CONFIG_FILE, {
        connections: [],
      });

      routerProcess = spawn('run');

      rpcClient = new RpcClient(client);
      await tryConnect(client, { path: LAUNCHER_PIPE });
    });

    it('sorts connections according to name and port', async () => {
      await rpcClient.addSerialPort({ serial_port: 'COM3' });
      await rpcClient.addSerialPort({ serial_port: 'COM4' });
      await rpcClient.addSerialPort({ serial_port: 'COM1' });
      await rpcClient.addSerialPort({ serial_port: 'COM2' });

      let infos = await rpcClient.listConnections();
      expect(infos.map(info => info.serial_port)).to.deep.eq(['COM1', 'COM2', 'COM3', 'COM4']);

      await rpcClient.setConnectionName('COM3', 'bravo');
      await rpcClient.setConnectionName('COM4', 'alpha');

      infos = await rpcClient.listConnections();
      expect(infos.map(info => info.serial_port)).to.deep.eq(['COM4', 'COM3', 'COM1', 'COM2']);
    });

    afterEach(async () => {
      await client.close();

      await routerProcess.end();
    });
  });

  describe('name', () => {
    beforeEach(async () => {
      await writeYaml(CONFIG_FILE, {
        name: 'Pepe-PC',
      });

      routerProcess = spawn('run');

      rpcClient = new RpcClient(client);
      await tryConnect(client, { path: LAUNCHER_PIPE });
    });

    afterEach(async () => {
      await client.close();

      await routerProcess.end();
    });

    it('gets the name from config', async () => {
      expect(await rpcClient.getName()).to.eq('Pepe-PC');
    });

    it('sets the name', async () => {
      await rpcClient.setName('Lola-PC');

      const config = await readYaml(CONFIG_FILE);
      expect(config.name).to.eq('Lola-PC');

      expect(await rpcClient.getName()).to.eq('Lola-PC');
    });
  });
});
