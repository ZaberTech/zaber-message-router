import Bluebird from 'bluebird';
import { expect } from 'chai';
import os from 'os';
import { createServer, Server } from 'net';
import MDNS, { MDNSResponse, SRVResponseData } from 'multicast-dns';
import _ from 'lodash';

import { LAUNCHER_PIPE } from './constants';
import { LocalShareStatus, RpcClient } from './rpc_client';
import { RouterProcess, spawn } from './run';
import { TcpClient } from './tcp';
import { cleanConfig, tryConnect } from './utils';

const LOCAL_SHARE_PORT = 22779;
const MAX_CONNECTION_WAIT_SECONDS = 5;
const MDNS_CANDIDATE_SERVICE_SUFFIX = `_zaber_message_router._tcp.local`;
const NAME = 'Lola-PC';
const INSTANCE_NAME = `test-id-${Math.random()}`.replace('.', '');

export interface LocalShareCandidate {
  serviceName: string;
  port?: number;
  host?: string;
  data?: Record<string, string>;
}

async function waitForLocalShare(rpcClient: RpcClient, desired: Partial<LocalShareStatus>): Promise<void> {
  for (let i = MAX_CONNECTION_WAIT_SECONDS; i > 0; i--) {
    const status = await rpcClient.getLocalShareStatus();
    if (_.isMatch(status, desired)) {
      break;
    }
    await Bluebird.delay(1000);
  }
}

async function startDummyTCPServer(port: number): Promise<Server> {
  const server = createServer();
  return new Promise<Server>(resolve => {
    server.listen(port, '127.0.0.1', () => resolve(server));
  });
}

function mdnsCandidateDiscovery(search: string): Promise<LocalShareCandidate[]> {
  const candidates: LocalShareCandidate[] = [];
  const handler = (response: MDNSResponse) => {
    response.answers.concat(response.additionals)
      .filter(record => record.name.endsWith(search))
      .forEach(record => {
        let candidate = candidates.find(el => el.serviceName === record.name);
        if (!candidate) {
          candidate = {
            serviceName: record.name,
          };
          candidates.push(candidate);
        }

        if (record.type === 'SRV') {
          const data = record.data as SRVResponseData;
          candidate.host = data.target;
          candidate.port = data.port;
        } else if (record.type === 'TXT') {
          candidate.data = _.fromPairs((record.data as Buffer[]).map(buffer => buffer.toString('ascii').split('=')));
        }
      });
  };

  const mdns = MDNS();
  mdns.on('response', handler);
  mdns.query({ questions: [{ name: search, type: 'PTR' }] });

  return new Promise<LocalShareCandidate[]>(resolve => setTimeout(() => {
    mdns.off('response', handler);
    mdns.destroy();
    resolve(candidates);
  }, 1000));
}

function findOwnMdnsRecord(records: LocalShareCandidate[]): LocalShareCandidate {
  return records.find(record => record.host === `${os.hostname().split('.')[0]}.local` && record.port === LOCAL_SHARE_PORT) || null;
}

describe('Local share', () => {
  let messageRouterProcess: RouterProcess;
  let rpcClientLocal: RpcClient;

  const tcpClientLocal = new TcpClient();

  beforeEach(async () => {
    await cleanConfig();

    messageRouterProcess = spawn('run');

    rpcClientLocal = new RpcClient(tcpClientLocal);
    await tryConnect(tcpClientLocal, { path: LAUNCHER_PIPE });

    await rpcClientLocal.setName(NAME);
  });

  afterEach(async () => {
    await tcpClientLocal.close();
    await messageRouterProcess.end();
    await cleanConfig();
  });

  it('Doesn\'t run if local share setting is null', async () => {
    await rpcClientLocal.setLocalShareConfig(null);
    await waitForLocalShare(rpcClientLocal, { is_running: false });

    const status = await rpcClientLocal.getLocalShareStatus();
    expect(status.is_running).to.eq(false);
    expect(status.is_discoverable).to.eq(false);

    const config = await rpcClientLocal.getLocalShareConfig();
    expect(config).to.eq(null);
  });

  describe('enabled', () => {
    const localShareSettings = {
      enabled: true,
      port: LOCAL_SHARE_PORT,
      discoverable: false,
      instance_name: INSTANCE_NAME,
    };

    beforeEach(async () => {
      await rpcClientLocal.setLocalShareConfig(localShareSettings);
      await waitForLocalShare(rpcClientLocal, { is_running: true });
    });

    it('Returns running status and previously set config', async () => {
      const status = await rpcClientLocal.getLocalShareStatus();
      expect(status.is_running).to.eq(true);
      expect(status.is_discoverable).to.eq(false);

      const config = await rpcClientLocal.getLocalShareConfig();
      expect(config).to.deep.eq(localShareSettings);
    });

    it('Stops when "enabled" setting is set to false', async () => {
      await rpcClientLocal.setLocalShareConfig({
        ...localShareSettings,
        enabled: false,
      });
      await waitForLocalShare(rpcClientLocal, { is_running: false });
      const status = await rpcClientLocal.getLocalShareStatus();
      expect(status.is_running).to.eq(false);
    });

    it('Communicates over local share when it is running', async () => {
      const tcpClientLocalShare = new TcpClient();
      const rpcClientLocalShare = new RpcClient(tcpClientLocalShare);
      await tryConnect(tcpClientLocalShare, { host: 'localhost', port: LOCAL_SHARE_PORT });

      await rpcClientLocalShare.statusPing();

      await tcpClientLocalShare.close();
    });

    it('closes all clients when local share is disabled', async () => {
      const tcpClientLocalShare = new TcpClient();
      const rpcClientLocalShare = new RpcClient(tcpClientLocalShare);
      await tryConnect(tcpClientLocalShare, { host: 'localhost', port: LOCAL_SHARE_PORT });

      await rpcClientLocalShare.statusPing();

      const disconnectPromise = tcpClientLocalShare.waitForClosing();

      await rpcClientLocal.setLocalShareConfig({
        ...localShareSettings,
        enabled: false,
      });

      await disconnectPromise;
    });
  });

  describe('discoverable', () => {
    beforeEach(async () => {
      await rpcClientLocal.setLocalShareConfig({
        enabled: true,
        port: LOCAL_SHARE_PORT,
        discoverable: true,
        instance_name: INSTANCE_NAME,
      });
      await waitForLocalShare(rpcClientLocal, { is_discoverable: true });
    });

    it('returns status indicating discoverable', async () => {
      const status = await rpcClientLocal.getLocalShareStatus();
      expect(status.is_discoverable).to.eq(true);
    });

    it('can be discovered by mDNS, returning info', async () => {
      const candidates = await mdnsCandidateDiscovery(MDNS_CANDIDATE_SERVICE_SUFFIX);

      const ownRecord = findOwnMdnsRecord(candidates);
      expect(ownRecord).to.not.eq(null);
      expect(ownRecord.serviceName).startWith(INSTANCE_NAME);
      expect(ownRecord.data.name).startWith(NAME);
    });

    it ('starts returning new name after change', async () => {
      await rpcClientLocal.setName('Pepe-PC');

      const candidates = await mdnsCandidateDiscovery(MDNS_CANDIDATE_SERVICE_SUFFIX);
      const ownRecord = findOwnMdnsRecord(candidates);
      expect(ownRecord.data.name).startWith('Pepe-PC');
    });
  });

  it('Is not discoverable when "discoverable" setting is set to false', async () => {
    await rpcClientLocal.setLocalShareConfig({
      enabled: true,
      port: LOCAL_SHARE_PORT,
      discoverable: false,
      instance_name: INSTANCE_NAME,
    });
    await waitForLocalShare(rpcClientLocal, { is_running: true });

    const status = await rpcClientLocal.getLocalShareStatus();
    expect(status.is_discoverable).to.eq(false);

    const candidates = await mdnsCandidateDiscovery(MDNS_CANDIDATE_SERVICE_SUFFIX);
    expect(findOwnMdnsRecord(candidates)).to.eq(null);
  });

  it('Is not discoverable when "enabled" setting is set to false', async () => {
    await rpcClientLocal.setLocalShareConfig({
      enabled: false,
      port: LOCAL_SHARE_PORT,
      discoverable: true,
      instance_name: INSTANCE_NAME,
    });
    await waitForLocalShare(rpcClientLocal, { is_running: false });

    const status = await rpcClientLocal.getLocalShareStatus();
    expect(status.is_discoverable).to.eq(false);

    const candidates = await mdnsCandidateDiscovery(MDNS_CANDIDATE_SERVICE_SUFFIX);
    expect(findOwnMdnsRecord(candidates)).to.eq(null);
  });

  describe('Port Collisions', () => {
    let server: Server;
    before(async () => {
      server = await startDummyTCPServer(LOCAL_SHARE_PORT);
    });
    after(() => {
      server.close();
    });

    /**
     * Does not work for Mac yet. See internal\localshare\localshare_darwin.go
     */
    (os.platform() !== 'darwin' ? it : _.noop)('Doesn\'t run and has error if port is already being used', async () => {
      await rpcClientLocal.setLocalShareConfig({
        enabled: true,
        port: LOCAL_SHARE_PORT,
        discoverable: true,
        instance_name: INSTANCE_NAME,
      });
      await waitForLocalShare(rpcClientLocal, { is_running: true });
      const status = await rpcClientLocal.getLocalShareStatus();
      expect(status.is_running).to.eq(false);
      expect(status.is_discoverable).to.eq(false);
      expect(status.last_run_error).to.startWith(`Unable to start local share service: listen tcp :${LOCAL_SHARE_PORT}: bind:`);
    });
  });
});
