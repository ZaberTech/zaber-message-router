declare module 'simple-jsonrpc-js';

declare module 'multicast-dns' {
  export interface MDNSAnswer {
    name: string;
    type: string;
    ttl: number;
    class: string,
    flush: boolean,
    data: Buffer[] | SRVResponseData;
  }

  export interface MDNSResponse {
    answers: MDNSAnswer[];
    additionals: MDNSAnswer[];
  }

  export interface SRVResponseData {
    priority: number;
    weight: number;
    port: number;
    target: string;
  }

  export interface MDNS extends NodeJS.EventEmitter {
    on(event: 'response', callback: (response: MDNSResponse) => void): this;
    query(query: unknown): void;
    destroy(): void;
  }

  export default function (): MDNS;
}

declare module '@folder/xdg' {
  export default function (options: { platform: string }): { cache: string; config: string; data: string; runtime: string };
}
