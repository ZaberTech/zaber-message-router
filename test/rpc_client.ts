import JsonRpc from 'simple-jsonrpc-js';
import { Subject, Observable } from 'rxjs';
import { take, filter, map } from 'rxjs/operators';

interface RcpClientIO {
  pushLine(line: string): Promise<void>;
  lines: Observable<string>;
}

interface Reply {
  connection: string;
  replies: string[];
  id: number;
}

export interface MonitorMessage {
  l: string;
  s: 'cmd' | 'cmd-other' | 'reply' | 'reply-other' | 'broadcast';
}
export interface MonitorMessageBundle {
  id: number;
  connection: string;
  messages: MonitorMessage[];
}

interface Error {
  connection: string;
  error: string;
}

interface CloudConfig {
  id: string;
  api_url: string;
  realm: string;
  token: string;
}

interface LocalShareConfig {
  enabled: boolean;
  port: number;
  discoverable: boolean;
  instance_name: string;
}
export interface LocalShareStatus {
  is_running: boolean;
  last_run_error?: string;
  is_discoverable: boolean;
  last_discoverability_error?: string;
}

export class RpcClient {
  private rpc: any;

  private _replies = new Subject<Reply>();
  public get replies(): Observable<Reply> {
    return this._replies;
  }
  private _errors = new Subject<Error>();
  public get errors(): Observable<Error> {
    return this._errors;
  }
  private _monitors = new Subject<MonitorMessageBundle>();
  public get monitors(): Observable<MonitorMessageBundle> {
    return this._monitors;
  }

  public nextMessageID = 0;

  constructor(private readonly io: RcpClientIO) {
    this.rpc = new JsonRpc();
    this.rpc.toStream = (message: any) => this.io.pushLine(message);

    this.io.lines.subscribe(line => this.rpc.messageHandler(line));

    this.rpc.on('routing_reply', ['connection', 'replies', 'id'], this.onReplies.bind(this));
    this.rpc.on('routing_error', ['connection', 'error'], this.onError.bind(this));
    this.rpc.on('routing_monitorMessage', ['connection', 'message', 'direction', 'id'], this.onMonitor.bind(this));
  }

  public async statusPing(): Promise<void> {
    return await this.rpc.call('status_ping', []);
  }

  public async statusName(): Promise<string> {
    return await this.rpc.call('status_getName', []);
  }

  public async statusApiVersion(): Promise<string> {
    return await this.rpc.call('status_getApiVersion', []);
  }

  public async controlExit(): Promise<string> {
    return await this.rpc.notification('control_exit', []);
  }

  public async warmupConnection(connectionID: string): Promise<void> {
    return await this.rpc.call('routing_warmup', [connectionID]);
  }
  public async cooldownConnection(connectionID: string): Promise<void> {
    return await this.rpc.call('routing_cooldown', [connectionID]);
  }

  public async startMonitoring(connectionID: string): Promise<void> {
    return await this.rpc.call('routing_startMonitoring', [connectionID]);
  }
  public async stopMonitoring(connectionID: string): Promise<void> {
    return await this.rpc.call('routing_stopMonitoring', [connectionID]);
  }

  private onReplies(connection: string, replies: string | string[], id: number): void {
    this._replies.next({
      connection,
      replies: Array.isArray(replies) ? replies : [replies],
      id,
    });
  }

  private onError(connection: string, error: string): void {
    this._errors.next({ connection, error });
  }

  private onMonitor(connection: string, messages: MonitorMessage[], id: number): void {
    this._monitors.next({ connection, messages, id });
  }

  public async sendMessage(connectionID: string, message: string, id?: number): Promise<number> {
    if (id == null) {
      id = this.nextMessageID;
      this.nextMessageID++;
    }
    return await this.rpc.notification('routing_sendMessage', [connectionID, message, id]);
  }

  public waitForReply(connectionID: string): Promise<string> {
    return this._replies.pipe(
      filter(reply => reply.connection === connectionID),
      map(({ replies }) => replies[0]),
      take(1),
    ).toPromise();
  }

  public async addSerialPort(config: { serial_port: string, baud_rate?: number }): Promise<any> {
    return await this.rpc.call('config_addSerialPort', [config]);
  }

  public async addTcpIp(config: { hostname: string, tcp_port?: number }): Promise<any> {
    return await this.rpc.call('config_addTCPIP', [config]);
  }

  public async listConnections(): Promise<any[]> {
    return await this.rpc.call('routing_listConnections');
  }

  public async removeConnection(connectionID: string): Promise<void> {
    await this.rpc.call('config_removeConnection', [connectionID]);
  }

  public async setCloudConfig(config: CloudConfig): Promise<void> {
    return await this.rpc.call('config_setCloudConfig', [config]);
  }

  public async getCloudConfig(): Promise<CloudConfig> {
    return await this.rpc.call('config_getCloudConfig');
  }

  public async getCloudStatus(): Promise<{ is_connected: boolean }> {
    return await this.rpc.call('cloud_getStatus');
  }

  public async setLocalShareConfig(config: LocalShareConfig): Promise<void> {
    await this.rpc.call('config_setLocalShareConfig', [config]);
  }

  public async getLocalShareConfig(): Promise<LocalShareConfig> {
    return await this.rpc.call('config_getLocalShareConfig');
  }

  public async getLocalShareStatus(): Promise<LocalShareStatus> {
    return await this.rpc.call('localshare_getStatus');
  }

  public async setName(name: string): Promise<void> {
    return await this.rpc.call('config_setName', [name]);
  }

  public async setConnectionName(connectionID: string, name: string): Promise<void> {
    await this.rpc.call('config_setConnectionName', [connectionID, name]);
  }

  public async getName(): Promise<string> {
    return await this.rpc.call('config_getName');
  }

  public async setClientVersion(version: string): Promise<string> {
    return await this.rpc.call('client_setVersion', [version]);
  }
}
