import { expect } from 'chai';
import { promises as fs } from 'fs';

import { readYaml } from './yml';
import { run } from './run';
import { cleanConfig, CONFIG_FILE } from './utils';

describe('Config', () => {
  beforeEach(async () => {
    await cleanConfig();
  });
  afterEach(async () => {
    await cleanConfig();
  });

  describe('add-serial', () => {
    it('adds port to config file', async () => {
      await run('config add-serial --serial-port COM3');

      const config = await readYaml(CONFIG_FILE);
      expect(config.connections).to.deep.eq([{
        type: 'serial',
        serial_port: 'COM3',
        baud_rate: 115200,
        name: '',
      }]);
    });

    it('allows baud rate override', async () => {
      await run('config add-serial --serial-port COM3 --baud-rate 19200');

      const config = await readYaml(CONFIG_FILE);
      expect(config.connections).to.deep.eq([{
        type: 'serial',
        serial_port: 'COM3',
        baud_rate: 19200,
        name: '',
      }]);
    });

    it('throws error when serial ports are not unique', async () => {
      await run('config add-serial --serial-port COM3');

      const promise = run('config add-serial --serial-port COM3');
      await expect(promise).to.be.rejectedWith();

      const { stderr } = await promise.catch(e => e);

      expect(stderr).to.contain('Serial port COM3 is used in the config more than once');
    });
  });

  it('lists serial ports', async () => {
    await run('config add-serial --serial-port COM2 --baud-rate 19200');
    await run('config add-serial --serial-port COM3');
    await run('config add-serial --serial-port COM4');
    const { stdout } = await run('config list');

    const table = stdout.trim().match(/^Using config file: [^\n]*\n(.*)$/s)[1];

    expect(table.trim()).to.eq(`
+-------------+-----------+
| Serial Port | Baud Rate |
+-------------+-----------+
| COM2        |     19200 |
| COM3        |    115200 |
| COM4        |    115200 |
+-------------+-----------+
`.trim()
    );
  });

  describe('add-tcp', () => {
    it('adds host to the config file', async () => {
      await run('config add-tcp --hostname localhost --tcp-port 6789');

      const config = await readYaml(CONFIG_FILE);
      expect(config.connections).to.deep.eq([{
        type: 'tcp/ip',
        hostname: 'localhost',
        tcp_port: 6789,
        name: '',
      }]);
    });

    it('throws error when host/port combination is not unique', async () => {
      await run('config add-tcp --hostname localhost --tcp-port 6789');

      await run('config add-tcp --hostname localhost --tcp-port 6790');

      const promise = run('config add-tcp --hostname localhost --tcp-port 6789');
      await expect(promise).to.be.rejectedWith();

      const { stderr } = await promise.catch(e => e);

      expect(stderr).to.contain('Hostname and port combination localhost:6789 is used in the config more than once');
    });
  });

  it('lists TCP ports', async () => {
    await run('config add-tcp --hostname localhost --tcp-port 6789');
    await run('config add-tcp --hostname 127.0.0.1 --tcp-port 1234');
    await run('config add-tcp --hostname kodo-pc --tcp-port 6789');
    const { stdout } = await run('config list');

    const table = stdout.trim().match(/^Using config file: [^\n]*\n(.*)$/s)[1];

    expect(table.trim()).to.eq(`
+-----------+------+
| Hostname  | Port |
+-----------+------+
| 127.0.0.1 | 1234 |
| kodo-pc   | 6789 |
| localhost | 6789 |
+-----------+------+
`.trim()
    );
  });

  describe('remove', () => {
    beforeEach(async () => {
      await run('config add-serial --serial-port COM3');
      await run('config add-serial --serial-port COM4');
      await run('config add-tcp --hostname localhost --tcp-port 6789');
      await run('config add-tcp --hostname localhost --tcp-port 6790');
      await run('config add-tcp --hostname kodo-pc --tcp-port 6789');
    });

    it('removes connections by serial port name', async () => {
      await run('config remove --serial-port COM4');

      const config = await readYaml(CONFIG_FILE);
      const connections = config.connections.map((connection: any) => connection.serial_port || connection.hostname);
      expect(connections).to.deep.eq(['COM3', 'kodo-pc', 'localhost', 'localhost']);
    });

    it('removes connections ports by hostname', async () => {
      await run('config remove --hostname localhost');

      const config = await readYaml(CONFIG_FILE);
      const connections = config.connections.map((connection: any) => connection.serial_port || connection.hostname);
      expect(connections).to.deep.eq(['COM3', 'COM4', 'kodo-pc']);
    });

    it('removes connections ports by hostname and port number', async () => {
      await run('config remove --hostname localhost --tcp-port 6789');

      const config = await readYaml(CONFIG_FILE);
      const connections = config.connections.map((connection: any) => connection.serial_port || connection.hostname);
      expect(connections).to.deep.eq(['COM3', 'COM4', 'kodo-pc', 'localhost']);
    });
  });

  it('prints warning when cannot parse file', async () => {
    await fs.writeFile(CONFIG_FILE, '!', 'utf8');

    const { stdout } = await run('config list');
    expect(stdout).to.contain('WARNING: Error when parsing config file');
  });

});
