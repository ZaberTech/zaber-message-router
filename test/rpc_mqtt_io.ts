import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Mqtt } from './mqtt';

export class RpcMqttIo {
  public linesSubject = new Subject<string>();
  public get lines(): Observable<string> {
    return this.linesSubject;
  }

  constructor(
    private readonly client: Mqtt,
    private readonly routerId: string,
    private readonly myId: string,
    private readonly realm: string) {
  }

  public async init(): Promise<void> {
    const topic = `${this.realm}/message-router/${this.myId}/${this.routerId}/rpc`;
    const messages = await this.client.subscribe(topic);
    messages.pipe(map(message => message.payload)).subscribe(this.linesSubject);
  }

  public async pushLine(line: string): Promise<void> {
    const topic = `${this.realm}/message-router/${this.routerId}/${this.myId}/rpc`;
    await this.client.publish(topic, line);
  }
}
