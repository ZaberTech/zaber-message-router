import { promises as fs } from 'fs';
import path from 'path';
import os from 'os';
import { delay } from 'bluebird';

import { TcpClient } from './tcp';
import { SocketConnectOpts } from 'net';

export const CONFIG_FILE = path.join(__dirname, 'message-router-config.yml');

export async function cleanConfig(): Promise<void> {
  try {
    await fs.unlink(CONFIG_FILE);
  } catch (err) { } // tslint:disable-line
}

export async function tryConnect(client: TcpClient, connectOpts: SocketConnectOpts, options: { attempts?: number; } = {}): Promise<void> {
  let lastError: any;

  for (let attempt = options.attempts || 20; attempt > 0; attempt--) {
    try {
      await client.connect(connectOpts);
      return;
    } catch (err) {
      lastError = err;

      if (!['ECONNREFUSED', 'ENOENT'].includes(err.code)) {
        break;
      }

      await delay(500);
    }
  }

  throw lastError;
}

export const itLinux64 = os.arch() === 'x64' && os.platform() === 'linux' ? it : xit;
export const beforeLinux64 = os.arch() === 'x64' && os.platform() === 'linux' ? before : () => '';
export const afterLinux64 = os.arch() === 'x64' && os.platform() === 'linux' ? after : () => '';
export const itCI = process.env.CI ? it : xit;

export function getID(message: string): number {
  return +(message.match(/\/\d \d (\d+)/)[1]);
}
