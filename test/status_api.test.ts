import { expect } from 'chai';
import { tryConnect } from './utils';
import { spawn, RouterProcess } from './run';
import { TcpClient } from './tcp';
import { LAUNCHER_PIPE } from './constants';
import { RpcClient } from './rpc_client';

describe('Status API', () => {
  const client = new TcpClient();
  let rpcClient: RpcClient;
  let routerProcess: RouterProcess;

  beforeEach(async () => {
    routerProcess = spawn('run');

    rpcClient = new RpcClient(client);
    await tryConnect(client, { path: LAUNCHER_PIPE });

    await rpcClient.setName('Lola-PC');
  });

  afterEach(async () => {
    await client.close();

    await routerProcess.end();
  });

  it('replies to ping', async () => {
    await rpcClient.statusPing();
  });

  it('returns name', async () => {
    expect(await rpcClient.statusName()).to.eq('Lola-PC');
  });

  it('returns API version', async () => {
    expect(await rpcClient.statusApiVersion()).to.match(/^\d+\.\d+$/);
  });
});
