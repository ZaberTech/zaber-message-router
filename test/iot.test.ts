import { expect } from 'chai';
import Bluebird from 'bluebird';
import { interval, Observable } from 'rxjs';
import { filter, map, flatMap, takeUntil, toArray, skip, take } from 'rxjs/operators';

import { RouterProcess, spawn } from './run';
import { cleanConfig, CONFIG_FILE, itCI, tryConnect } from './utils';
import { TcpClient } from './tcp';
import { RpcClient } from './rpc_client';
import { API_URL, LAUNCHER_PIPE, UNAUTHENTICATED_REALM, UNAUTHENTICATED_TOKEN } from './constants';
import { Mqtt } from './mqtt';
import { RpcMqttIo } from './rpc_mqtt_io';
import { readYaml, writeYaml } from './yml';
import { TcpServer, TEST_PORT } from './tcp_server';

const ROUTER_ID = `router-test-${Math.random()}`.replace('.', '');
const NAME = 'Lola-PC';
const WAIT_FOR_CLOUD = 15;
const TEST_CLOUD_CONFIG = {
  api_url: API_URL,
  id: ROUTER_ID,
  realm: UNAUTHENTICATED_REALM,
  token: UNAUTHENTICATED_TOKEN,
};

async function waitForCloud(rpcClient: RpcClient, connected: boolean = true): Promise<void> {
  for (let i = WAIT_FOR_CLOUD; i > 0; i--) {
    const status = await rpcClient.getCloudStatus();
    if (status.is_connected === connected) {
      break;
    }
    await Bluebird.delay(1000);
  }
}

describe('IoT', () => {
  let routerProcess: RouterProcess;
  const client = new TcpClient();
  let rpcClientLocal: RpcClient;
  let rpcClientIot: RpcClient;
  const mqtt = new Mqtt();

  before(async () => {
    await mqtt.connect(UNAUTHENTICATED_TOKEN);
    const mqttIo = new RpcMqttIo(mqtt, ROUTER_ID, 'router-test', UNAUTHENTICATED_REALM);
    await mqttIo.init();
    rpcClientIot = new RpcClient(mqttIo);
  });
  after(async () => {
    mqtt.disconnect();
  });

  beforeEach(async () => {
    await writeYaml(CONFIG_FILE, {
      connections: [{
        type: 'tcp/ip',
        hostname: 'localhost',
        tcp_port: TEST_PORT,
      }],
    });

    routerProcess = spawn('run');

    rpcClientLocal = new RpcClient(client);
    await tryConnect(client, { path: LAUNCHER_PIPE });

    await rpcClientLocal.setName(NAME);
  });
  afterEach(async () => {
    await client.close();

    await routerProcess.end();

    await cleanConfig();
  });

  it('connects to cloud', async function(): Promise<void> {
    this.timeout(20000);

    let status = await rpcClientLocal.getCloudStatus();
    expect(status.is_connected).to.eq(false);

    await rpcClientLocal.setCloudConfig(TEST_CLOUD_CONFIG);
    await waitForCloud(rpcClientLocal);

    status = await rpcClientLocal.getCloudStatus();
    expect(status.is_connected).to.eq(true);
  });

  it('does not break when cloud config set to null while it\'s null', async () => {
    await rpcClientLocal.setCloudConfig(null);
    const status = await rpcClientLocal.getCloudStatus();
    expect(status.is_connected).to.eq(false);
  });

  describe('cloud connected', () => {
    beforeEach(async function(): Promise<void> {
      this.timeout(20000);

      await rpcClientLocal.setCloudConfig(TEST_CLOUD_CONFIG);
      await waitForCloud(rpcClientLocal);
    });

    it('communicates over cloud', async () => {
      await rpcClientIot.statusPing();
    });

    it('returns the config back and keeps it in config file', async () => {
      expect(await rpcClientLocal.getCloudConfig()).to.deep.eq(TEST_CLOUD_CONFIG);
      const config = await readYaml(CONFIG_FILE);
      expect(config.cloud).to.deep.eq(TEST_CLOUD_CONFIG);
    });

    it('allows to change the configuration reconnecting to cloud', async function(): Promise<void> {
      this.timeout(20000);

      const secondID = ROUTER_ID.replace('test', 'test2');

      await rpcClientLocal.setCloudConfig({
        ...TEST_CLOUD_CONFIG,
        id: secondID,
      });
      await waitForCloud(rpcClientLocal);

      const mqttIoNewId = new RpcMqttIo(mqtt, secondID, 'router-test', UNAUTHENTICATED_REALM);
      await mqttIoNewId.init();
      const rpcClientIotNewId = new RpcClient(mqttIoNewId);

      await rpcClientIotNewId.statusPing();
    });

    it('disconnects from cloud when cloud config set null', async function(): Promise<void> {
      this.timeout(20000);

      await rpcClientLocal.setCloudConfig(null);
      await waitForCloud(rpcClientLocal, false);

      const status = await rpcClientLocal.getCloudStatus();
      expect(status.is_connected).to.eq(false);

      expect(await rpcClientLocal.getCloudConfig()).to.eq(null);
    });

    describe('discovery', () => {
      let discoveryResponses: Observable<{ id: string }>;
      beforeEach(async () => {
        const discovery = await mqtt.subscribe(`${UNAUTHENTICATED_REALM}/message-router/discovery-response`);
        discoveryResponses = discovery.pipe(
          map(message => JSON.parse(message.payload)),
          filter(message => message.id === ROUTER_ID),
        );
      });

      it('replies to discovery request', async () => {
        const responsePromise = discoveryResponses.pipe(take(1)).toPromise();
        await interval(2000).pipe(
          flatMap(() => mqtt.publish(`${UNAUTHENTICATED_REALM}/message-router/discovery-request`, '')),
          takeUntil(discoveryResponses)
        ).toPromise();

        expect(await responsePromise).to.deep.eq({
          id: ROUTER_ID,
          name: NAME,
        });
      });

      itCI('replies to request no more often than every 5 seconds', async function(): Promise<void> {
        this.timeout(20 * 1000);
        const secondResponse = discoveryResponses.pipe(skip(1));
        const requestsSent = await interval(1000).pipe(
          flatMap(() => mqtt.publish(`${UNAUTHENTICATED_REALM}/message-router/discovery-request`, '')),
          takeUntil(secondResponse),
          toArray()
        ).toPromise();
        expect(requestsSent.length).to.be.gte(5);
      });
    });

    describe('routed connection', () => {
      const server = new TcpServer();
      const CONNECTION_ID = `localhost:${TEST_PORT}`;

      before(async () => {
        await server.start();
      });
      after(async () => {
        await server.stop();
      });

      beforeEach(async () => {
        await rpcClientIot.setClientVersion('2.0');
        await rpcClientIot.warmupConnection(CONNECTION_ID);
        await server.waitForConnection();
      });

      afterEach(async () => {
        server.closeSocket();
      });

      it('batches info messages sent over IoT', async () => {
        // this test relies on router receiving the messages within 100ms timeout
        const replies = rpcClientIot.replies.pipe(take(1)).toPromise();

        await server.push('#1 0 stuff1');
        await server.push('#1 0 stuff2');

        expect((await replies).replies).to.deep.eq([
          '#1 0 stuff1',
          '#1 0 stuff2',
        ]);
      });
    });
  });
});
