package main

import (
	_ "gitlab.izaber.com/software-public/message-router/internal/init"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

func main() {
	ioc.Instance.GetApp().Run()
}
