[:arrow_left: Index](README.md)

# Zaber Message Router Changelog

## 2024-07-10, Version 0.1.9
* Fixing IoT packets getting too large

## 2024-01-11, Version 0.1.8
* Adds binaries for Apple Silicon

## 2023-03-14, Version 0.1.7
* Fixing half open connection upon process being stuck

## 2022-10-20, Version 0.1.6
* Fixing IoT packets dropping when device sends a lot of info messages

## 2022-04-12, Version 0.1.5
* Support for line continuations

## 2022-03-22, Version 0.1.4
* Renaming pipe to support multiple users

## 2021-12-10, Version 0.1.3
* Naming connections

## 2019-09-24, Version 0.1.2
* Improvements to IoT stability

## 2019-09-08, Version 0.1.1
* **Release**
