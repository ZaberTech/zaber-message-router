package service

import (
	"os"
	"runtime"
	"strings"

	"github.com/kardianos/service"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
	cli "gopkg.in/urfave/cli.v1"
)

type serviceImpl struct {
	run           ioc.Run
	config        ioc.Config
	service       service.Service
	serviceConfig *service.Config
	control       ioc.RunControl
}

var statusStrings = map[service.Status]string{
	service.StatusUnknown: "unknown",
	service.StatusRunning: "running",
	service.StatusStopped: "stopped",
}

func NewService(app ioc.App, run ioc.Run, config ioc.Config) ioc.Service {
	instance := &serviceImpl{
		run:    run,
		config: config,
	}

	app.AddCommand(&cli.Command{
		Name:  "service",
		Usage: "Installs and control this application as a system service.",
		Subcommands: []cli.Command{
			{
				Name:   "install",
				Usage:  "Installs the service.",
				Before: instance.before,
				Action: func(_ *cli.Context) error {
					return instance.controlCmd("install")
				},
				Flags: instance.getInstallFlags(),
			},
			{
				Name:   "uninstall",
				Usage:  "Uninstalls the service",
				Before: instance.before,
				Action: func(_ *cli.Context) error {
					return instance.controlCmd("uninstall")
				},
			},
			{
				Name:   "start",
				Usage:  "Starts the service",
				Before: instance.before,
				Action: func(_ *cli.Context) error {
					return instance.controlCmd("start")
				},
			},
			{
				Name:   "stop",
				Usage:  "Stops the service",
				Before: instance.before,
				Action: func(_ *cli.Context) error {
					return instance.controlCmd("stop")
				},
			},
			{
				Name:   "restart",
				Usage:  "Restarts the service",
				Before: instance.before,
				Action: func(_ *cli.Context) error {
					return instance.controlCmd("restart")
				},
			},
			{
				Name:   "status",
				Usage:  "Prints status of the service",
				Before: instance.before,
				Action: func(_ *cli.Context) error {
					return instance.status()
				},
			},
			{
				Name:   "run",
				Usage:  "Runs the service",
				Before: instance.before,
				Action: instance.runService,
				Hidden: true,
				Flags: []cli.Flag{
					cli.StringFlag{
						Name: "config-file",
					},
				},
			},
		},
	})

	return run
}

func (instance *serviceImpl) Start(_ service.Service) error {
	instance.control = instance.run.CreateControl()

	go instance.run.Run(instance.control)
	instance.control.WaitForStart()

	return nil
}
func (instance *serviceImpl) Stop(_ service.Service) error {
	instance.control.Stop()
	instance.control.WaitForStop()

	return nil
}

func (instance *serviceImpl) runService(c *cli.Context) error {
	if configFile := c.String("config-file"); configFile != "" {
		instance.config.SetFile(configFile)
	}

	if logger, err := instance.service.SystemLogger(nil); err == nil {
		out.SetLogger(logger)
	} else {
		out.Warn("Cannot initialize system log: %s", err.Error())
	}

	return instance.service.Run()
}

func (instance *serviceImpl) controlCmd(cmd string) error {
	if err := service.Control(instance.service, cmd); err != nil {
		ignoreErr := false

		if runtime.GOOS == "darwin" {
			errText := strings.TrimSpace(err.Error())
			ignoreErr = cmd == "stop" && (strings.HasSuffix(errText, ": Operation now in progress") ||
				strings.HasSuffix(errText, ": Could not find specified service"))
		}

		if !ignoreErr {
			return err
		}
	}

	out.Print("%s successful.", cmd)
	return nil
}

func (instance *serviceImpl) status() error {
	status, err := instance.service.Status()
	if err == nil {
		out.Print("Status: %s", statusStrings[status])
	} else {
		out.Fatal("Error when getting status: %s", err)
	}
	return nil
}

func (instance *serviceImpl) before(c *cli.Context) error {
	instance.serviceConfig = createServiceConfig(c)

	newService, err := service.New(instance, instance.serviceConfig)
	if err == service.ErrNoServiceSystemDetected {
		out.Fatal("No service system detected. Cannot run as a service on this system.")
	} else if err != nil {
		return err
	}

	instance.service = newService
	return nil
}

func createServiceConfig(c *cli.Context) *service.Config {
	isRoot := os.Getuid() == 0

	svcConfig := &service.Config{
		Name:        "zaber-message-router",
		DisplayName: "Zaber Message Router",
		Option:      service.KeyValue{},
	}

	if runtime.GOOS == "windows" {
		svcConfig.Description = "Zaber Message Router facilitates connection between devices (serial port, TCP) and applications"
	} else {
		// possibly a bug, description appears in logs instead of the name
		svcConfig.Description = svcConfig.Name
	}

	if runtime.GOOS == "darwin" {
		svcConfig.Option["UserService"] = !isRoot
	}

	if c.Command.Name == "install" {
		fillConfigForInstallation(svcConfig, c)
	}

	return svcConfig
}

func fillConfigForInstallation(svcConfig *service.Config, c *cli.Context) {
	isRoot := os.Getuid() == 0

	svcConfig.Arguments = []string{
		"service",
		"run",
	}

	if configFile := c.String("config-file"); configFile != "" {
		svcConfig.Arguments = append(svcConfig.Arguments, "--config-file", configFile)
	}

	if user := c.String("user"); user == "" && isRoot {
		out.Fatal("Please specify user that will run the service")
	}

	switch runtime.GOOS {
	case "linux":
		if !isRoot {
			out.Fatal("Please run the commands as root")
		}
		if user := c.String("user"); user != "" {
			svcConfig.UserName = c.String("user")
		}

	case "darwin":
		svcConfig.Option["KeepAlive"] = true
		svcConfig.Option["RunAtLoad"] = true

		if user := c.String("user"); user != "" {
			if isRoot {
				svcConfig.UserName = c.String("user")
			} else {
				out.Fatal("The --user is not supported for non-root users")
			}
		}

	case "windows":
		if user := c.String("user"); user != "" {
			svcConfig.UserName = user
			svcConfig.Option["Password"] = c.String("password")
		}
	}
}

func (instance *serviceImpl) getInstallFlags() []cli.Flag {
	var installFlags []cli.Flag

	isRoot := os.Getuid() == 0

	if runtime.GOOS == "windows" {
		installFlags = append(installFlags, cli.StringFlag{
			Name:  "user, u",
			Usage: "Optionally specifies user to run the service (defaults to Local System account)",
		})
		installFlags = append(installFlags, cli.StringFlag{
			Name:  "password, p",
			Usage: "Provides password for the user (if specified)",
		})
	} else if isRoot {
		installFlags = append(installFlags, cli.StringFlag{
			Name:  "user, u",
			Usage: "Specifies user that runs the service (required)",
		})
	}

	if isRoot {
		installFlags = append(installFlags, cli.StringFlag{
			Name:  "config-file",
			Usage: "Optionally specifies file with configuration (defaults to configuration of the user)",
		})
	} else {
		installFlags = append(installFlags, cli.StringFlag{
			Name:  "config-file",
			Usage: "Optionally specifies file with configuration",
			Value: instance.config.GetDefaultFile(),
		})
	}

	return installFlags
}
