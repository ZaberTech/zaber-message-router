package config

import (
	"encoding/json"
	"fmt"
	"math"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"sync"

	texttable "github.com/syohex/go-texttable"
	"gitlab.izaber.com/software-public/message-router/internal/constants"
	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
	cli "gopkg.in/urfave/cli.v1"
	yaml "gopkg.in/yaml.v2"
)

const configFilePathEnv = "ZABER_MESSAGE_ROUTER_CONFIG_FILE"

type configImpl struct {
	configFile string
	root       *dto.ConfigRoot
	lock       sync.Mutex
}

type flags struct {
	hostname   string
	serialPort string
	baudRate   uint
	tcpPort    uint
}

var specifiedFlags flags

func NewConfig(app ioc.App) ioc.Config {
	config := &configImpl{
		configFile: getConfigFilePath(),
	}

	app.AddCommand(&cli.Command{
		Name:    "config",
		Aliases: []string{"cfg"},
		Usage:   "Configure Zaber launcher service",
		Before: func(_ *cli.Context) error {
			config.readConfig()
			out.Print("")
			return nil
		},
		Subcommands: []cli.Command{
			{
				Name:  "add-serial",
				Usage: "Adds new serial port connection",
				Action: func(_ *cli.Context) error {
					return config.addSerial()
				},
				Flags: []cli.Flag{
					cli.StringFlag{
						Name:        "serial-port, s",
						Usage:       "Specifies the mapped serial port. Required.",
						Destination: &specifiedFlags.serialPort,
					},
					cli.UintFlag{
						Name:        "baud-rate, b",
						Usage:       "Optionally specifies the baud rate used when comunicating on the serial port.",
						Value:       constants.BaudRateASCII,
						Destination: &specifiedFlags.baudRate,
					},
				},
			},
			{
				Name:  "add-tcp",
				Usage: "Adds new TCP/IP connection",
				Action: func(_ *cli.Context) error {
					return config.addTCP()
				},
				Flags: []cli.Flag{
					cli.StringFlag{
						Name:        "hostname, host",
						Usage:       "Specifies the hostname or IP address of the device. Required.",
						Destination: &specifiedFlags.hostname,
					},
					cli.UintFlag{
						Name:        "tcp-port, p",
						Usage:       "Specifies the TCP port of the connection. Required.",
						Destination: &specifiedFlags.tcpPort,
					},
				},
			},
			{
				Name:  "list",
				Usage: "Lists configuration",
				Action: func(c *cli.Context) error {
					return config.list(c)
				},
			},
			{
				Name:  "remove",
				Usage: "Removes a connection",
				Action: func(c *cli.Context) error {
					return config.remove(c)
				},
				Flags: []cli.Flag{
					cli.StringFlag{
						Name:        "serial-port, s",
						Usage:       "Specifies the serial port to be removed.",
						Destination: &specifiedFlags.serialPort,
					},
					cli.StringFlag{
						Name:        "hostname, host",
						Usage:       "Specifies the hostname or IP address to be removed.",
						Destination: &specifiedFlags.hostname,
					},
					cli.UintFlag{
						Name:        "tcp-port, p",
						Usage:       "Specifies the TCP port in addition to the hostname of the connection to be removed. If not given, all connections to the host will be removed.",
						Destination: &specifiedFlags.tcpPort,
					},
				},
			},
		},
	})

	return config
}
func (config *configImpl) addSerial() error {
	if len(specifiedFlags.serialPort) == 0 {
		out.Fatal("--serial-port not specified")
	}

	if _, err := config.AddSerial(specifiedFlags.serialPort, specifiedFlags.baudRate); err != nil {
		out.Fatale(err)
	}

	config.printMapping()

	out.Print("New connection added.")

	return nil
}

func (config *configImpl) addTCP() error {
	if len(specifiedFlags.hostname) == 0 {
		out.Fatal("--hostname not specified")
	}
	if specifiedFlags.tcpPort == 0 {
		out.Fatal("--tcp-port not specified")
	}

	if _, err := config.AddTCPIP(specifiedFlags.hostname, specifiedFlags.tcpPort); err != nil {
		out.Fatale(err)
	}

	config.printMapping()

	out.Print("New connection added.")

	return nil
}

func (config *configImpl) list(_ *cli.Context) error {
	config.printMapping()

	return nil
}

func (config *configImpl) remove(_ *cli.Context) error {
	newConfig := cloneConfig(config.root)

	var filtered []*dto.Connection
	removed := 0

	for _, connection := range newConfig.Connections {
		toBeRemoved := false
		if len(specifiedFlags.serialPort) > 0 && connection.SerialPort == specifiedFlags.serialPort {
			toBeRemoved = true
		}
		if len(specifiedFlags.hostname) > 0 && connection.Hostname == specifiedFlags.hostname &&
			(specifiedFlags.tcpPort == 0 || specifiedFlags.tcpPort == connection.TCPPort) {
			toBeRemoved = true
		}
		if toBeRemoved {
			removed++
		} else {
			filtered = append(filtered, connection)
		}
	}

	newConfig.Connections = filtered

	if err := validateAndSortConnections(newConfig); err != nil {
		out.Fatale(err)
	}
	if err := config.swapConfig(newConfig); err != nil {
		out.Fatale(err)
	}

	config.printMapping()

	out.Print("Removed %d connections.", removed)

	return nil
}

func validateAndSortConnections(config *dto.ConfigRoot) error {
	uniqueSerial := make(map[string]bool)
	uniqueHostPort := make(map[string]bool)

	connections := config.Connections

	for _, connection := range connections {
		if connection.Type == dto.ConnectionTypeSerial {
			if _, found := uniqueSerial[connection.SerialPort]; found {
				return fmt.Errorf("Serial port %s is used in the config more than once", connection.SerialPort)
			}
			uniqueSerial[connection.SerialPort] = true
		} else if connection.Type == dto.ConnectionTypeTCPIP {
			hostPort := fmt.Sprintf("%s:%d", connection.Hostname, connection.TCPPort)
			if _, found := uniqueHostPort[hostPort]; found {
				return fmt.Errorf("Hostname and port combination %s is used in the config more than once", hostPort)
			}
			uniqueHostPort[hostPort] = true
		}
	}

	sort.Slice(connections, func(i, j int) bool {
		if connections[i].Type != connections[j].Type {
			return connections[i].Type < connections[j].Type
		}
		if connections[i].Name != "" || connections[j].Name != "" {
			if connections[i].Name == "" {
				return false
			} else if connections[j].Name == "" {
				return true
			} else {
				return connections[i].Name < connections[j].Name
			}
		}
		if connections[i].Type == dto.ConnectionTypeSerial && connections[i].SerialPort != connections[j].SerialPort {
			return connections[i].SerialPort < connections[j].SerialPort
		}
		if connections[i].Type == dto.ConnectionTypeTCPIP {
			if connections[i].Hostname != connections[j].Hostname {
				return connections[i].Hostname < connections[j].Hostname
			}
			return connections[i].TCPPort < connections[j].TCPPort
		}
		return false
	})

	return nil
}

func cloneConfig(config *dto.ConfigRoot) *dto.ConfigRoot {
	newConfig := &dto.ConfigRoot{}
	data, _ := json.Marshal(config)
	_ = json.Unmarshal(data, newConfig)
	return newConfig
}

func (config *configImpl) printMapping() {
	if len(config.root.Connections) == 0 {
		out.Print("No connections configured.")
		return
	}

	count := 0
	table := &texttable.TextTable{}
	_ = table.SetHeader("Serial Port", "Baud Rate")

	for _, connection := range config.root.Connections {
		if connection.Type != dto.ConnectionTypeSerial {
			continue
		}
		_ = table.AddRow(connection.SerialPort, fmt.Sprintf("%d", connection.BaudRate))
		count++
	}

	if count > 0 {
		out.Print(table.Draw())
	}

	count = 0
	table = &texttable.TextTable{}
	_ = table.SetHeader("Hostname", "Port")

	for _, connection := range config.root.Connections {
		if connection.Type != dto.ConnectionTypeTCPIP {
			continue
		}
		_ = table.AddRow(connection.Hostname, fmt.Sprintf("%d", connection.TCPPort))
		count++
	}

	if count > 0 {
		out.Print(table.Draw())
	}
}

func (config *configImpl) readConfig() {
	out.Print("Using config file: %s", config.configFile)

	config.root = &dto.ConfigRoot{}

	data, err := os.ReadFile(config.configFile)
	if err != nil {
		if os.IsNotExist(err) {
			out.Warn("Config file does not exist.")
		} else {
			out.Warn("Error when reading config file: %s", err.Error())
		}
		return
	}

	var root dto.ConfigRoot
	err = yaml.Unmarshal(data, &root)
	if err != nil {
		out.Warn("Error when parsing config file: %s", err.Error())
		return
	}
	config.root = &root
}

func (config *configImpl) writeConfig(configRoot *dto.ConfigRoot) error {
	data, err := yaml.Marshal(configRoot)
	if err != nil {
		return fmt.Errorf("Cannot serialize config: %s", err.Error())
	}

	directory := filepath.Dir(config.configFile)
	if err := os.MkdirAll(directory, 0700); err != nil {
		return fmt.Errorf("Cannot create directories for config to file %s: %s", config.configFile, err.Error())
	}

	if err := os.WriteFile(config.configFile, data, 0600); err != nil {
		return fmt.Errorf("Cannot write config to file %s: %s", config.configFile, err.Error())
	}

	return nil
}

func (config *configImpl) swapConfig(newConfig *dto.ConfigRoot) error {
	if err := config.writeConfig(newConfig); err != nil {
		return err
	}
	config.root = newConfig
	return nil
}

func (config *configImpl) GetConfig() *dto.ConfigRoot {
	config.lock.Lock()
	defer config.lock.Unlock()

	if config.root == nil {
		config.readConfig()
	}

	return config.root
}

func (config *configImpl) GetDefaultFile() string {
	return getConfigFilePath()
}

func (config *configImpl) SetFile(configFile string) {
	config.lock.Lock()
	defer config.lock.Unlock()

	if config.root != nil {
		panic(fmt.Errorf("Settings already loaded"))
	}

	config.configFile = configFile
}

func (config *configImpl) AddSerial(port string, baudRate uint) (*dto.Connection, error) {
	config.lock.Lock()
	defer config.lock.Unlock()

	if baudRate == 0 {
		baudRate = constants.BaudRateASCII
	}

	newConnection := &dto.Connection{
		Type:       dto.ConnectionTypeSerial,
		SerialPort: strings.TrimSpace(port),
		BaudRate:   baudRate,
	}
	if len(newConnection.SerialPort) == 0 {
		return nil, fmt.Errorf("Serial port not specified")
	}

	newConfig := cloneConfig(config.root)
	newConfig.Connections = append(newConfig.Connections, newConnection)

	if err := validateAndSortConnections(newConfig); err != nil {
		return nil, err
	}
	if err := config.swapConfig(newConfig); err != nil {
		return nil, err
	}
	return newConnection, nil
}

func (config *configImpl) AddTCPIP(hostname string, port uint) (*dto.Connection, error) {
	config.lock.Lock()
	defer config.lock.Unlock()

	newConnection := &dto.Connection{
		Type:     dto.ConnectionTypeTCPIP,
		Hostname: strings.TrimSpace(hostname),
		TCPPort:  port,
	}
	if len(newConnection.Hostname) == 0 {
		return nil, fmt.Errorf("Hostname not specified")
	}
	if newConnection.TCPPort <= 0 || math.MaxUint16 < newConnection.TCPPort {
		return nil, fmt.Errorf("Invalid TCP port: %d", newConnection.TCPPort)
	}

	newConfig := cloneConfig(config.root)
	newConfig.Connections = append(newConfig.Connections, newConnection)

	if err := validateAndSortConnections(newConfig); err != nil {
		return nil, err
	}
	if err := config.swapConfig(newConfig); err != nil {
		return nil, err
	}
	return newConnection, nil
}

var validCloudIDRegexp, _ = regexp.Compile(`^[a-zA-Z0-9_\-:]+$`)
var validCloudRealmRegexp = validCloudIDRegexp

func (config *configImpl) SetCloudConfig(cloudConfig *dto.CloudConfig) error {
	config.lock.Lock()
	defer config.lock.Unlock()

	if cloudConfig != nil {
		if !validCloudIDRegexp.MatchString(cloudConfig.ID) {
			return fmt.Errorf("Cloud ID is empty or contains invalid characters")
		}
		if !validCloudRealmRegexp.MatchString(cloudConfig.Realm) {
			return fmt.Errorf("Cloud realm is empty or contains invalid characters")
		}
		if len(cloudConfig.Token) == 0 {
			return fmt.Errorf("Cloud token is empty")
		}
		if _, err := url.Parse(cloudConfig.ApiUrl); err != nil {
			return fmt.Errorf("Api Url is invalid: %s", err.Error())
		}
	}

	newConfig := cloneConfig(config.root)
	newConfig.Cloud = cloudConfig

	return config.swapConfig(newConfig)
}

var validLocalShareInstanceNameRegexp = validCloudIDRegexp

func (config *configImpl) SetLocalShareConfig(localShareConfig *dto.LocalShareConfig) error {
	config.lock.Lock()
	defer config.lock.Unlock()

	if localShareConfig != nil {
		if !validLocalShareInstanceNameRegexp.MatchString(localShareConfig.InstanceName) {
			return fmt.Errorf("Instance Name is empty or contains invalid characters")
		}
	}

	newConfig := cloneConfig(config.root)
	newConfig.LocalShare = localShareConfig

	return config.swapConfig(newConfig)
}

func (config *configImpl) RemoveConnection(identifier dto.ConnectionID) error {
	config.lock.Lock()
	defer config.lock.Unlock()

	newConfig := cloneConfig(config.root)

	var filtered []*dto.Connection
	for _, connection := range newConfig.Connections {
		if dto.MakeIdentifier(connection) != identifier {
			filtered = append(filtered, connection)
		}
	}
	if len(newConfig.Connections) == len(filtered) {
		return fmt.Errorf("Cannot find connection %s", identifier)
	}
	newConfig.Connections = filtered

	if err := validateAndSortConnections(newConfig); err != nil {
		return err
	}
	return config.swapConfig(newConfig)
}

var validNameRegexp, _ = regexp.Compile(`\S`)

func (config *configImpl) SetName(name string) error {
	config.lock.Lock()
	defer config.lock.Unlock()

	if !validNameRegexp.MatchString(name) {
		return fmt.Errorf("Name must not be empty")
	}

	newConfig := cloneConfig(config.root)
	newConfig.Name = strings.TrimSpace(name)

	return config.swapConfig(newConfig)
}

func (config *configImpl) SetConnectionName(identifier dto.ConnectionID, name string) error {
	config.lock.Lock()
	defer config.lock.Unlock()

	newConfig := cloneConfig(config.root)
	foundConnection := false

	for _, connection := range newConfig.Connections {
		if dto.MakeIdentifier(connection) == identifier {
			connection.Name = strings.TrimSpace(name)
			foundConnection = true
			break
		}
	}

	if !foundConnection {
		return fmt.Errorf("Cannot find connection %s", identifier)
	}

	if err := validateAndSortConnections(newConfig); err != nil {
		return err
	}
	return config.swapConfig(newConfig)
}
