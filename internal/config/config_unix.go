//go:build !windows

package config

import (
	"os"
	"path/filepath"
)

const configFileName = "config.yml"
const configFolder = ".zaber-message-router"
const alternateConfig = "/etc/zaber-message-router.yml"

func getConfigFilePath() string {
	if envPath := os.Getenv(configFilePathEnv); envPath != "" {
		return envPath
	}

	if configHome := os.Getenv("XDG_CONFIG_HOME"); configHome != "" && filepath.IsAbs(configHome) {
		return filepath.Join(configHome, configFolder, configFileName)
	}

	if home, err := os.UserHomeDir(); err == nil {
		return filepath.Join(home, ".config", configFolder, configFileName)
	}

	return alternateConfig
}
