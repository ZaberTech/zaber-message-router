//go:build windows

package config

import (
	"os"
	"path/filepath"
)

const configFileName = "config.yml"
const alternateFileName = "zaber-message-router-config.yml"

func getConfigFilePath() string {
	if envPath := os.Getenv(configFilePathEnv); envPath != "" {
		return envPath
	}

	appData := os.Getenv("LOCALAPPDATA")
	if appData != "" && filepath.IsAbs(appData) {
		return filepath.Join(appData, "Zaber Technologies", "Zaber Message Router", configFileName)
	}

	if exeFile, err := os.Executable(); err == nil {
		return filepath.Join(filepath.Dir(exeFile), alternateFileName)
	}

	return alternateFileName
}
