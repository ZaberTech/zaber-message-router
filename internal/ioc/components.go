package ioc

import (
	"io"
	"net"
	"time"

	baseUtils "gitlab.com/ZaberTech/zaber-go-lib/pkg/utils"
	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/utils"
	cli "gopkg.in/urfave/cli.v1"
)

type App interface {
	AddCommand(cmd *cli.Command)
	AddFlag(flag cli.Flag)
	Run()
}

type Config interface {
	GetConfig() *dto.ConfigRoot
	GetDefaultFile() string
	SetFile(configFile string)

	AddSerial(port string, baudRate uint) (*dto.Connection, error)
	AddTCPIP(hostname string, port uint) (*dto.Connection, error)
	RemoveConnection(identifier dto.ConnectionID) error
	SetCloudConfig(cloudConfig *dto.CloudConfig) error
	SetLocalShareConfig(cloudConfig *dto.LocalShareConfig) error
	SetName(name string) error
	SetConnectionName(identifier dto.ConnectionID, name string) error
}

type RunControl interface {
	Stop()
	WaitForStart()
	WaitForStop()
}

type Run interface {
	Run(control RunControl)
	CreateControl() RunControl
}

type Tools interface {
	ListPorts() error
	ListIPAddresses() error
	DisplayHostname() error
}

type Service interface {
}

type RoutingClient interface {
	IpcClient
	SendReplies(fromConnectionID dto.ConnectionID, messages []string, id uint32)
	SendError(fromConnectionID dto.ConnectionID, err error)
	MonitorMessage(fromConnectionID dto.ConnectionID, messages []dto.MonitorMessage, id uint32)

	// client v1.0 API
	SendReplyV1(fromConnectionID dto.ConnectionID, message string, id uint32)
}

type RoutingConnection interface {
	Warmup(client RoutingClient) error
	Cooldown(client RoutingClient) error
	SendMessage(fromClient RoutingClient, message string, id uint32)
	StartMonitoring(client RoutingClient) error
	StopMonitoring(client RoutingClient) error
}

type Routing interface {
	Start() error
	Stop()
	GetConnection(connectionID dto.ConnectionID) (RoutingConnection, error)

	AddConnection(connection *dto.Connection) error
	RemoveConnection(connectionID dto.ConnectionID) error
}

type CodecIpcClient interface {
	io.Closer
	SetWriteDeadline(time.Time) error
	Encode(v interface{}) error
	Decode(v interface{}) error
}

type Ipc interface {
	Start(errCtx utils.ErrContext, runControl RunControl) error
	Stop()
	NewCodecClient(client CodecIpcClient, clientType ClientType)
	NewCodecConnection(conn net.Conn, clientType ClientType, finished chan interface{})
}

type Cloud interface {
	IsConnected() bool
	ReloadConfig()
	Start()
	Stop()
	SetIPC(ipc Ipc)
}

type LocalShare interface {
	Start()
	Stop()
	ReloadConfig()
	ReloadName()
	SetIPC(ipc Ipc)
	GetRunningStatus() (bool, error)
	GetDiscoverabilityStatus() (bool, error)
}

type SingleInstance interface {
	Ensure() error
	Release()
}

type ClientType int

const (
	ClientTypeLocal ClientType = iota
	ClientTypeNetwork
	ClientTypeIoT
)

type IpcClient interface {
	ClientType() ClientType
	Version() baseUtils.Version
	Closed() chan interface{}
}
