package localshare

import (
	"fmt"
	"net"
	"sync"

	"github.com/ethereum/go-ethereum/p2p/netutil"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
)

type localShareService struct {
	config ioc.Config
	ipc    ioc.Ipc

	lock sync.Mutex

	tcpListener  net.Listener
	stopped      chan bool
	isRunning    bool
	lastRunError error

	connectionsLock sync.Mutex
	connections     map[net.Conn]bool

	discovery *localShareDiscovery
}

func NewLocalShare(config ioc.Config) ioc.LocalShare {
	localShare := &localShareService{
		config:    config,
		discovery: NewDiscovery(config),
	}
	return localShare
}

func (service *localShareService) Start() {
	service.lock.Lock()
	defer service.lock.Unlock()

	if service.isRunning {
		out.Print("Local share already started.")
		return
	}

	service.lastRunError = nil

	configRoot := service.config.GetConfig()
	config := configRoot.LocalShare
	if config == nil {
		out.Print("Local share not configured. Skipping.")
		return
	}
	if !config.Enabled {
		out.Print("Local share disabled in configuration. Service not started.")
		return
	}

	listener, err := createTCPListener(config.Port)
	if err != nil {
		err = fmt.Errorf("Unable to start local share service: %s", err)
		service.lastRunError = err
		out.Warn(err.Error())
		return
	}

	service.stopped = make(chan bool)
	service.connections = make(map[net.Conn]bool)
	service.tcpListener = listener
	service.isRunning = true

	go service.clientConnectionLoop(listener, service.stopped)

	service.discovery.Start()
}

func (service *localShareService) Stop() {
	service.lock.Lock()
	defer service.lock.Unlock()

	service.discovery.Stop()

	if service.tcpListener != nil {
		close(service.stopped)
		if err := service.tcpListener.Close(); err != nil {
			out.Warn("Cannot close TCP listener: %s", err)
		}

		service.tcpListener = nil
		service.stopped = nil
	}

	service.connectionsLock.Lock()
	for connection := range service.connections {
		closeConnection(connection)
	}
	service.connections = nil
	service.connectionsLock.Unlock()

	service.isRunning = false
}

func (service *localShareService) ReloadConfig() {
	service.Stop()
	service.Start()
}

func (service *localShareService) SetIPC(ipc ioc.Ipc) {
	service.ipc = ipc
}

func (service *localShareService) clientConnectionLoop(listener net.Listener, stopped chan bool) {
	for {
		clientConnection, err := listener.Accept()
		if err != nil {
			select {
			case <-stopped:
				return
			default:
				if netutil.IsTemporaryError(err) {
					continue
				}

				err = fmt.Errorf("Unable to accept a client connection: %s", err)

				service.lock.Lock()
				service.lastRunError = err
				service.lock.Unlock()

				service.Stop()

				out.Warn(err.Error())
				return
			}
		}

		service.connectionsLock.Lock()
		isStopped := service.connections == nil
		if !isStopped {
			service.connections[clientConnection] = true
		}
		service.connectionsLock.Unlock()

		if isStopped {
			closeConnection(clientConnection)
			return
		}

		connectionFinished := make(chan interface{})
		service.ipc.NewCodecConnection(clientConnection, ioc.ClientTypeNetwork, connectionFinished)

		go func() {
			<-connectionFinished
			service.connectionCleanup(clientConnection)
		}()

		out.Print("New local share client: %s", clientConnection.RemoteAddr().String())
	}
}

func (service *localShareService) GetRunningStatus() (bool, error) {
	service.lock.Lock()
	defer service.lock.Unlock()
	return service.isRunning, service.lastRunError
}

func (service *localShareService) connectionCleanup(connection net.Conn) {
	service.connectionsLock.Lock()
	delete(service.connections, connection)
	service.connectionsLock.Unlock()

	closeConnection(connection)

	out.Print("Local share client disconnected: %s", connection.RemoteAddr().String())
}

func closeConnection(connection net.Conn) {
	if err := connection.Close(); err != nil {
		out.Warn("Error when closing client: %s", err)
	}
}

func (service *localShareService) ReloadName() {
	service.discovery.Restart()
}

func (service *localShareService) GetDiscoverabilityStatus() (bool, error) {
	return service.discovery.GetDiscoverabilityStatus()
}
