//go:build darwin

package localshare

import (
	"context"
	"fmt"
	"net"
	"syscall"
)

func createTCPListener(port uint) (net.Listener, error) {
	listenerConfig := &net.ListenConfig{
		Control: createConnControl,
	}
	return listenerConfig.Listen(context.TODO(), "tcp", fmt.Sprintf(":%d", port))
}

func createConnControl(_, _ string, _ syscall.RawConn) error {
	/* This does not work at this moment as it does not allows to reopen the socket after closing it.
	We need to understand it and fix it. */
	return nil

	/* We need to start the socket with the `SO_REUSEADDR` option set to 0 on darwin (OSX) so
	that we get an error if we end up trying to take ownership of a port that is already owned
	by some other process. For some reason, `SO_REUSEADDR` option may be getting set to 1 for
	some processes by default on OSX.
	*/
	/*return conn.Control(func(fd uintptr) {
		err := syscall.SetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 0)
		if err != nil {
			panic(err)
		}
	})*/
}
