package localshare

import (
	"net"
	"os"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/grandcat/zeroconf"
	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

const mdnsServiceSuffix = "_zaber_message_router._tcp"
const discoveryMonitorInterfacesPeriod = 60 * time.Second

type localShareDiscovery struct {
	config ioc.Config

	lock       sync.Mutex
	statusLock sync.Mutex

	mdnsService    *zeroconf.Server
	isDiscoverable bool
	lastError      error

	restart chan bool

	stopMainLoop    chan interface{}
	mainLoopRunning sync.WaitGroup
}

func NewDiscovery(config ioc.Config) *localShareDiscovery {
	return &localShareDiscovery{
		config:  config,
		restart: make(chan bool, 1),
	}
}

func (service *localShareDiscovery) Start() {
	service.lock.Lock()
	defer service.lock.Unlock()

	if alreadyRunning := service.stopMainLoop != nil; alreadyRunning {
		return
	}

	service.statusLock.Lock()
	service.lastError = nil
	service.statusLock.Unlock()

	service.stopMainLoop = make(chan interface{})
	service.mainLoopRunning.Add(1)
	go service.mainLoop(service.stopMainLoop)
}

func (service *localShareDiscovery) Stop() {
	service.lock.Lock()
	defer service.lock.Unlock()

	if service.stopMainLoop != nil {
		close(service.stopMainLoop)
		service.stopMainLoop = nil
	}
	service.mainLoopRunning.Wait()
}

func (service *localShareDiscovery) Restart() {
	select {
	case service.restart <- true:
	default:
	}
}
func (service *localShareDiscovery) drainRestartChannel() {
	for {
		select {
		case <-service.restart:
		default:
			return
		}
	}
}

func (service *localShareDiscovery) GetDiscoverabilityStatus() (bool, error) {
	service.statusLock.Lock()
	defer service.statusLock.Unlock()
	return service.isDiscoverable, service.lastError
}

func (service *localShareDiscovery) startMDNS() (discoverable bool, err error) {
	configRoot := service.config.GetConfig()
	config := configRoot.LocalShare
	if !config.Enabled || !config.Discoverable {
		return false, nil
	}

	interfaces, err := listMulticastInterfaces()
	if err != nil {
		return false, err
	}
	if len(interfaces) == 0 {
		return true, nil
	}

	hostname, err := getLocalHostname()
	if err != nil {
		return false, err
	}

	mdnsService, err := zeroconf.RegisterProxy(
		config.InstanceName, mdnsServiceSuffix,
		"local.", int(config.Port), hostname, nil,
		getMDNSInfo(configRoot),
		interfaces)
	if err != nil {
		return false, err
	}

	service.mdnsService = mdnsService
	return true, nil
}

func (service *localShareDiscovery) stopMDNS() {
	if service.mdnsService != nil {
		service.mdnsService.Shutdown()
		service.mdnsService = nil
	}

	service.statusLock.Lock()
	service.isDiscoverable = false
	service.statusLock.Unlock()
}

func (service *localShareDiscovery) mainLoop(stop chan interface{}) {
	defer service.mainLoopRunning.Done()
	defer service.stopMDNS()

	service.drainRestartChannel()

	restart := true
	var previousAddresses []string
	for {
		addresses, err := listInterfaceAddresses()
		if err != nil {
			service.statusLock.Lock()
			service.lastError = err
			service.statusLock.Unlock()
			continue
		}

		interfacesChanged := len(addresses) != len(previousAddresses)
		if !interfacesChanged {
			for i, address := range addresses {
				if address != previousAddresses[i] {
					interfacesChanged = true
					break
				}
			}
		}

		if interfacesChanged || restart {
			previousAddresses = addresses
			restart = false

			service.stopMDNS()

			isDiscoverable, err := service.startMDNS()
			service.statusLock.Lock()
			service.lastError = err
			service.isDiscoverable = isDiscoverable
			service.statusLock.Unlock()
		}

		select {
		case <-stop:
			return
		case <-service.restart:
			restart = true
		case <-time.After(discoveryMonitorInterfacesPeriod):
		}
	}
}

func listMulticastInterfaces() ([]net.Interface, error) {
	var interfaces []net.Interface
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	for _, iface := range ifaces {
		if (iface.Flags&net.FlagMulticast) > 0 && (iface.Flags&net.FlagLoopback) == 0 {
			interfaces = append(interfaces, iface)
		}
	}
	return interfaces, nil
}

func listInterfaceAddresses() ([]string, error) {
	addresses, err := net.InterfaceAddrs()
	if err != nil {
		return nil, err
	}
	addressesStr := make([]string, 0, len(addresses))
	for _, address := range addresses {
		if address.Network() != "ip+net" {
			continue
		}

		addressesStr = append(addressesStr, address.String())
	}
	sort.Strings(addressesStr)
	return addressesStr, nil
}

func getMDNSInfo(configRoot *dto.ConfigRoot) []string {
	return []string{"name=" + configRoot.Name}
}

func getLocalHostname() (string, error) {
	// returns FQDN on MacOS that we need to strip for zeroconf
	hostname, err := os.Hostname()
	if err != nil {
		return "", err
	}

	if splitIndex := strings.Index(hostname, "."); splitIndex >= 0 {
		hostname = hostname[:splitIndex]
	}
	return hostname, nil
}
