//go:build windows

package localshare

import (
	"context"
	"fmt"
	"net"
	"syscall"

	"golang.org/x/sys/windows"
)

func createTCPListener(port uint) (net.Listener, error) {
	listenerConfig := &net.ListenConfig{
		Control: createConnControl,
	}
	return listenerConfig.Listen(context.TODO(), "tcp", fmt.Sprintf(":%d", port))
}

func createConnControl(_, _ string, conn syscall.RawConn) error {
	/* We need to start the socket with the `SO_EXCLUSIVEADDRUSE` option set to 1 on windows so
	that we get an error if we end up trying to take ownership of a port that is already owned
	by some other process.
	Go doesn't provide us a value for `SO_EXCLUSIVEADDRUSE` natively so we need to compute it
	ourselves. `SO_EXCLUSIVEADDRUSE` value is defined in `WinSock2.h` as the bitwise NOT of
	`SO_REUSEADDR`
	*/
	return conn.Control(func(fd uintptr) {
		err := windows.SetsockoptInt(windows.Handle(fd), windows.SOL_SOCKET, ^windows.SO_REUSEADDR, 1)
		if err != nil {
			panic(err)
		}
	})
}
