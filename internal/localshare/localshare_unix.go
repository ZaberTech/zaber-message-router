//go:build !windows && !darwin

package localshare

import (
	"fmt"
	"net"
)

func createTCPListener(port uint) (net.Listener, error) {
	return net.Listen("tcp", fmt.Sprintf(":%d", port))
}
