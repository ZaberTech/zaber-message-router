package tools

import (
	"net"
	"os"

	serial "github.com/zabertech/go-serial"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
	cli "gopkg.in/urfave/cli.v1"
)

type toolsImpl struct{}

func NewTools(app ioc.App) ioc.Tools {
	tools := &toolsImpl{}

	app.AddCommand(&cli.Command{
		Name:  "tools",
		Usage: "Commands to list your serial ports, IP addresses.",
		Subcommands: []cli.Command{
			{
				Name:  "hostname",
				Usage: "Displays hostname of this computer.",
				Action: func(_ *cli.Context) error {
					return tools.DisplayHostname()
				},
			},
			{
				Name:  "ip",
				Usage: "Lists local IP addresses of this computer.",
				Action: func(_ *cli.Context) error {
					return tools.ListIPAddresses()
				},
			},
			{
				Name:  "list",
				Usage: "Lists serial ports present on this computer.",
				Action: func(_ *cli.Context) error {
					return tools.ListPorts()
				},
			},
		},
	})

	return tools
}

func (tools *toolsImpl) ListPorts() error {
	ports, err := serial.GetPortsList()
	if err != nil {
		out.Fatal("Error while listing serial ports: %s", err)
	}

	if len(ports) == 0 {
		out.Print("No serial ports found.")
	} else {
		out.Print("Found serial ports:")
		for _, port := range ports {
			out.Print("- %v", port)
		}
	}

	return nil
}

type ipWithInterface struct {
	iface net.Interface
	ip    net.IP
}

func (tools *toolsImpl) ListIPAddresses() error {
	ifaces, err := net.Interfaces()
	if err != nil {
		out.Warn("Cannot retrieve IP address information: %s", err.Error())
		return err
	}

	var lastIfaceErr error
	var ipAddresses []*ipWithInterface

	for _, iface := range ifaces {
		addresses, err := iface.Addrs()
		if err != nil {
			out.Warn("Cannot retrieve IP address information of interface %s: %s", iface.Name, err.Error())
			lastIfaceErr = err
			continue
		}

		for _, address := range addresses {
			var ip net.IP
			switch value := address.(type) {
			case *net.IPNet:
				ip = value.IP
			case *net.IPAddr:
				ip = value.IP
			}
			if ip.IsLinkLocalUnicast() || ip.IsLoopback() {
				continue
			}

			ipAddresses = append(ipAddresses, &ipWithInterface{
				iface: iface,
				ip:    ip,
			})
		}
	}

	if len(ipAddresses) > 0 {
		out.Print("Local IP addresses:")
		for _, address := range ipAddresses {
			out.Print("%s (%s)", address.ip.String(), address.iface.Name)
		}
	} else {
		out.Print("This computer does not seem to have an IP address assigned.")
	}

	return lastIfaceErr
}

func (tools *toolsImpl) DisplayHostname() error {
	hostname, err := os.Hostname()
	if err != nil {
		out.Warn("Cannot display hostname: %s", err.Error())
		return err
	}

	out.Print("Hostname: %s", hostname)
	return nil
}
