package utils

type ErrContext interface {
	Err() chan error
	ReportErr(err error)
}

type errContext struct {
	err chan error
}

func NewErrContext() ErrContext {
	return &errContext{
		err: make(chan error, 1),
	}
}

func (ctx *errContext) Err() chan error {
	return ctx.err
}

func (ctx *errContext) ReportErr(err error) {
	select {
	case ctx.err <- err:
	default:
	}
}
