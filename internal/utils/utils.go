package utils

import (
	"os"
	"time"
)

func GetCurrentWorkingDirectory() string {
	if dir, err := os.Getwd(); err == nil {
		return dir
	}
	return ""
}

func MaxDur(d1 time.Duration, d2 time.Duration) time.Duration {
	if d1 > d2 {
		return d1
	} else {
		return d2
	}
}
func MinDur(d1 time.Duration, d2 time.Duration) time.Duration {
	if d1 < d2 {
		return d1
	} else {
		return d2
	}
}
