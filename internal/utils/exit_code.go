package utils

const ExitCodeAnotherInstanceRunning = 2

type exitCodeErr struct {
	inner    error
	exitCode int
}

func NewExitCodeError(inner error, exitCode int) error {
	return &exitCodeErr{
		inner:    inner,
		exitCode: exitCode,
	}
}

func (err *exitCodeErr) Error() string {
	return err.inner.Error()
}

func IsExitCodeErr(err interface{}) (bool, int) {
	if exitCodeErr, ok := err.(*exitCodeErr); ok {
		return true, exitCodeErr.exitCode
	}
	return false, 0
}
