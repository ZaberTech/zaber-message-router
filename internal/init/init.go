package init

import (
	"gitlab.izaber.com/software-public/message-router/internal/app"
	"gitlab.izaber.com/software-public/message-router/internal/cloud"
	"gitlab.izaber.com/software-public/message-router/internal/config"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/ipc"
	"gitlab.izaber.com/software-public/message-router/internal/localshare"
	"gitlab.izaber.com/software-public/message-router/internal/routing"
	"gitlab.izaber.com/software-public/message-router/internal/run"
	"gitlab.izaber.com/software-public/message-router/internal/service"
	"gitlab.izaber.com/software-public/message-router/internal/singleinstance"
	"gitlab.izaber.com/software-public/message-router/internal/tools"
)

type container struct {
	app            ioc.App
	config         ioc.Config
	run            ioc.Run
	tools          ioc.Tools
	service        ioc.Service
	routing        ioc.Routing
	ipc            ioc.Ipc
	cloud          ioc.Cloud
	localShare     ioc.LocalShare
	singleInstance ioc.SingleInstance
}

func init() {
	container := &container{}

	container.app = app.NewApp()
	container.config = config.NewConfig(container.app)
	container.tools = tools.NewTools(container.app)
	container.routing = routing.NewRouting(container.config)
	container.cloud = cloud.NewCloud(container.config)
	container.localShare = localshare.NewLocalShare(container.config)
	container.ipc = ipc.NewIpc(container.routing, container.config, container.cloud, container.localShare)
	container.cloud.SetIPC(container.ipc)
	container.localShare.SetIPC(container.ipc)
	container.singleInstance = singleinstance.NewSingleInstance()
	container.run = run.NewRun(container.app, container.config, container.tools, container.routing, container.ipc, container.cloud, container.localShare, container.singleInstance)
	container.service = service.NewService(container.app, container.run, container.config)
	ioc.Instance = container
}

func (container *container) GetApp() ioc.App {
	return container.app
}
