package dto

const ConnectionTypeSerial = "serial"
const ConnectionTypeTCPIP = "tcp/ip"

type Connection struct {
	Type       string `yaml:"type"`
	SerialPort string `yaml:"serial_port,omitempty"`
	Hostname   string `yaml:"hostname,omitempty"`
	Name       string `yaml:"name"`
	BaudRate   uint   `yaml:"baud_rate,omitempty"`
	TCPPort    uint   `yaml:"tcp_port,omitempty"`
}

type ConfigRoot struct {
	Name        string            `yaml:"name"`
	Cloud       *CloudConfig      `yaml:"cloud"`
	LocalShare  *LocalShareConfig `yaml:"local_share"`
	Connections []*Connection     `yaml:"connections"`
}

type CloudConfig struct {
	ID     string `yaml:"id" json:"id"`
	ApiUrl string `yaml:"api_url" json:"api_url"` // revive:disable-line:var-naming
	Realm  string `yaml:"realm" json:"realm"`
	Token  string `yaml:"token" json:"token"`
}

type LocalShareConfig struct {
	Enabled      bool   `yaml:"enabled" json:"enabled"`
	Port         uint   `yaml:"port" json:"port"`
	Discoverable bool   `yaml:"discoverable" json:"discoverable"`
	InstanceName string `yaml:"instance_name" json:"instance_name"`
}

type MonitorMessage struct {
	Line   string `json:"l"`
	Source string `json:"s"`
}
