package dto

import "fmt"

type ConnectionID string

func MakeIdentifier(connection *Connection) ConnectionID {
	switch connection.Type {
	case ConnectionTypeSerial:
		return (ConnectionID)(connection.SerialPort)
	case ConnectionTypeTCPIP:
		return (ConnectionID)(fmt.Sprintf("%s:%d", connection.Hostname, connection.TCPPort))
	default:
		return "invalid_connection"
	}
}
