package run

import (
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

type runControl struct {
	termination chan os.Signal
	started     sync.WaitGroup
	stopped     sync.WaitGroup
}

func (run *runImpl) CreateControl() ioc.RunControl {
	ctrl := &runControl{
		termination: make(chan os.Signal, 1),
	}
	ctrl.started.Add(1)
	ctrl.stopped.Add(1)
	return ctrl
}

func (ctrl *runControl) WaitForStart() {
	ctrl.started.Wait()
}

func (ctrl *runControl) WaitForStop() {
	ctrl.stopped.Wait()
}

func (ctrl *runControl) Stop() {
	select {
	case ctrl.termination <- syscall.SIGTERM:
	default:
	}
}

func (ctrl *runControl) terminateOnSignal() {
	signal.Notify(ctrl.termination, os.Interrupt, syscall.SIGTERM)
}
