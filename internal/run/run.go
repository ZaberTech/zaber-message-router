package run

import (
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
	"gitlab.izaber.com/software-public/message-router/internal/utils"
	cli "gopkg.in/urfave/cli.v1"
)

type runImpl struct {
	config         ioc.Config
	tools          ioc.Tools
	routing        ioc.Routing
	ipc            ioc.Ipc
	cloud          ioc.Cloud
	localShare     ioc.LocalShare
	singleInstance ioc.SingleInstance
}

func NewRun(app ioc.App, config ioc.Config, tools ioc.Tools, routing ioc.Routing, ipc ioc.Ipc, cloud ioc.Cloud, localShare ioc.LocalShare, singleInstance ioc.SingleInstance) ioc.Run {
	run := &runImpl{
		config:         config,
		tools:          tools,
		routing:        routing,
		ipc:            ipc,
		cloud:          cloud,
		localShare:     localShare,
		singleInstance: singleInstance,
	}

	app.AddCommand(&cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Runs the application to forward data between network and serial ports.",
		Action: func(c *cli.Context) error {
			return run.runConsole(c)
		},
	})

	return run
}

func (run *runImpl) runConsole(_ *cli.Context) error {
	ctrl := run.CreateControl()
	ctrl.(*runControl).terminateOnSignal()

	run.Run(ctrl)

	return nil
}

func (run *runImpl) Run(control ioc.RunControl) {
	run.mainLoop(control)
}

func (run *runImpl) mainLoop(control ioc.RunControl) {
	out.Print("Starting...")

	ctrlImpl := control.(*runControl)
	errCtx := utils.NewErrContext()

	if err := run.singleInstance.Ensure(); err != nil {
		out.Fatal("Cannot ensure single instance: %s", err)
	}
	if err := run.routing.Start(); err != nil {
		out.Fatal("Cannot start routing: %s", err)
	}
	if err := run.ipc.Start(errCtx, control); err != nil {
		out.Fatal("Cannot start IPC: %s", err)
	}
	run.cloud.Start()
	run.localShare.Start()

	out.Print("Press Ctrl+C to stop...")

	ctrlImpl.started.Done()
	select {
	case <-ctrlImpl.termination:
	case err := <-errCtx.Err():
		out.Warn("Stopping due to an error: %s", err.Error())
	}

	out.Print("Stopping...")

	run.routing.Stop()
	run.cloud.Stop()
	run.localShare.Stop()
	run.ipc.Stop()
	run.singleInstance.Release()

	out.Print("Stopped")
	ctrlImpl.stopped.Done()
}
