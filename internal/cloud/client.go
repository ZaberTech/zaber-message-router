package cloud

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitlab.izaber.com/software-public/message-router/internal/out"
)

type rpcClientParent interface {
	sendRPCMessage(idFrom string, payload string) (mqtt.Token, error)
}

type rpcClient struct {
	id     string
	parent rpcClientParent
	lock   sync.Mutex

	rxLock    sync.Mutex
	rx        chan []byte
	rxClosedC chan interface{}
	rxClosed  bool

	writeTimeout    time.Duration
	lastMessageTime time.Time
	isClosed        bool
}

const defaultWriteTimeout = 30 * time.Second

func newClient(id string, parent rpcClientParent) *rpcClient {
	newClient := &rpcClient{
		id:              id,
		parent:          parent,
		rx:              make(chan []byte, 5),
		rxClosedC:       make(chan interface{}),
		writeTimeout:    defaultWriteTimeout,
		lastMessageTime: time.Now(),
	}
	return newClient
}

func (client *rpcClient) onRPCMessage(message mqtt.Message) {
	message.Ack()

	client.rxLock.Lock()
	rxClosed := client.rxClosed
	if !rxClosed {
		select {
		case <-client.rxClosedC:
			rxClosed = true
		case client.rx <- message.Payload():
		}
	}
	client.rxLock.Unlock()

	if rxClosed {
		return
	}

	client.lock.Lock()
	client.lastMessageTime = time.Now()
	client.lock.Unlock()
}

func (client *rpcClient) Decode(v interface{}) error {
	data := <-client.rx
	if data == nil {
		return fmt.Errorf("Connection closed")
	}
	return json.Unmarshal(data, v)
}

func (client *rpcClient) Encode(v interface{}) error {
	if err := client.send(v); err != nil {
		out.Warn("IoT send failed: %s", err.Error())
	}
	// Encode errors are ignored because clients themselves can detect missing messages.
	// Returning error here would cause the RPC client write to fail forever without being detected.
	// Alternatively we could close the RPC client but that would prevent the remote client detecting the missing message.
	return nil
}

func (client *rpcClient) send(v interface{}) error {
	stringer, ok := v.(fmt.Stringer)
	if !ok {
		return fmt.Errorf("Data is not stringer")
	}
	payload := stringer.String()

	token, err := client.parent.sendRPCMessage(client.id, payload)
	if err != nil {
		return err
	}
	if !token.WaitTimeout(client.writeTimeout) {
		return fmt.Errorf("Write timeout")
	}
	return token.Error()
}

func (client *rpcClient) SetWriteDeadline(writeDeadline time.Time) error {
	// This is incredibly bad concept, instead of timeout it gives you time in future for every encode call
	client.writeTimeout = time.Until(writeDeadline)
	return nil
}

func (client *rpcClient) Close() error {
	client.lock.Lock()
	isClosed := client.isClosed
	client.isClosed = true
	client.lock.Unlock()

	if isClosed {
		return nil
	}

	close(client.rxClosedC)
	client.rxLock.Lock()
	client.rxClosed = true
	client.rxLock.Unlock()
	close(client.rx)

	return nil
}

func (client *rpcClient) getLastMessageTime() time.Time {
	client.lock.Lock()
	defer client.lock.Unlock()

	return client.lastMessageTime
}

func (client *rpcClient) waitForClose() {
	<-client.rxClosedC
}

func (client *rpcClient) RemoteAddr() string {
	return client.id
}
