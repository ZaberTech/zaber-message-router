package cloud

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
)

// TODO: allow client to reconnect by action

const qos = 0
const clientInactiveTimeout = 10 * time.Minute
const qosDiscovery = 0
const discoveryResponseDebounce = 5 * time.Second

type cloudService struct {
	config       ioc.Config
	cloudConfig  *dto.CloudConfig
	ipc          ioc.Ipc
	stopped      chan bool
	reconnecting chan bool
	isConnected  bool
	isStopped    bool
	lock         sync.Mutex

	client        mqtt.Client
	clientOptions *mqtt.ClientOptions

	clients     map[string]*rpcClient
	clientsLock sync.Mutex

	cleanupTicker *time.Ticker

	lastDiscoveryResponse time.Time
}

func NewCloud(config ioc.Config) ioc.Cloud {
	cloud := &cloudService{
		config:        config,
		cleanupTicker: time.NewTicker(time.Minute),
		isStopped:     true,
	}
	return cloud
}

func (cloud *cloudService) SetIPC(ipc ioc.Ipc) {
	cloud.ipc = ipc
}

func (cloud *cloudService) IsConnected() bool {
	cloud.lock.Lock()
	defer cloud.lock.Unlock()
	return cloud.isConnected
}

func (cloud *cloudService) setMqttConfig() {
	options := mqtt.NewClientOptions()
	options.SetClientID(fmt.Sprintf("%s-%s", cloud.cloudConfig.Realm, cloud.cloudConfig.ID))
	options.SetProtocolVersion(4)
	options.SetConnectTimeout(10 * time.Second)
	// this mechanism only works once you are connected for the first time
	// and we need fresh URL every time
	// therefore it's not suitable for us
	options.SetAutoReconnect(false)
	options.SetOnConnectHandler(cloud.onConnected)
	options.SetConnectionLostHandler(cloud.onLost)
	cloud.clientOptions = options
}

func (cloud *cloudService) Start() {
	cloud.cloudConfig = cloud.config.GetConfig().Cloud
	if cloud.cloudConfig == nil {
		out.Print("Cloud not configured, skipping.")
		return
	}

	cloud.setMqttConfig()

	cloud.isStopped = false
	cloud.stopped = make(chan bool)
	cloud.clients = make(map[string]*rpcClient)
	cloud.reconnecting = make(chan bool)

	go cloud.connectLoop(cloud.reconnecting)
	go cloud.cleanUp()
}

func (cloud *cloudService) Stop() {
	cloud.stopClient()
}

func (cloud *cloudService) stopClient() {
	cloud.lock.Lock()

	reconnecting := cloud.reconnecting
	cloud.reconnecting = nil

	client := cloud.client
	cloud.client = nil

	wasStopped := cloud.isStopped
	cloud.isStopped = true
	cloud.isConnected = false

	cloud.lock.Unlock()

	if wasStopped {
		return
	}

	close(cloud.stopped)

	if client != nil {
		client.Disconnect(0)
	}

	cloud.clientsLock.Lock()
	clients := cloud.clients
	cloud.clients = nil
	cloud.clientsLock.Unlock()

	for _, rpcClient := range clients {
		rpcClient.Close()
	}

	<-reconnecting
}

func (cloud *cloudService) ReloadConfig() {
	cloud.stopClient()
	cloud.Start()
}

type urlResponse struct {
	URL string `json:"url"`
}

func (cloud *cloudService) getWebsocketURL() (string, error) {
	resp, err := http.Get(fmt.Sprintf("%s/iot-token/websocket-url?token=%s", cloud.cloudConfig.ApiUrl, cloud.cloudConfig.Token))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("Invalid status code: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	response := &urlResponse{}
	if err := json.Unmarshal(body, response); err != nil {
		return "", err
	}
	return response.URL, nil
}

func (cloud *cloudService) connectAttempt() error {
	url, err := cloud.getWebsocketURL()
	if err != nil {
		return fmt.Errorf("Could not obtain authentication token: %s", err)
	}

	cloud.clientOptions.Servers = nil
	cloud.clientOptions.AddBroker(url)

	cloud.lock.Lock()
	isStopped := cloud.isStopped
	if !isStopped {
		cloud.client = mqtt.NewClient(cloud.clientOptions)
	}
	client := cloud.client
	cloud.lock.Unlock()

	if isStopped {
		return fmt.Errorf("Client stopped")
	}

	token := client.Connect()
	token.Wait()
	return token.Error()
}

const maxReconnectTime = time.Minute

var urlInErrRegexp, _ = regexp.Compile("\"https:[^\"]+\"")

func (cloud *cloudService) connectLoop(reconnecting chan bool) {
	reconnectTime := time.Second
	for {
		err := cloud.connectAttempt()
		if err == nil {
			close(reconnecting)
			return
		}
		errWithoutToken := urlInErrRegexp.ReplaceAllString(err.Error(), "url")
		out.Warn("Cannot connect to cloud: %s", errWithoutToken)

		if reconnectTime < maxReconnectTime {
			reconnectTime *= 2
		}
		select {
		case <-cloud.stopped:
			close(reconnecting)
			return
		case <-time.After(reconnectTime):
		}
	}
}

func (cloud *cloudService) onConnected(mqttClient mqtt.Client) {
	out.Print("Connected to IoT")

	cloud.lock.Lock()
	isStopped := cloud.isStopped
	if !isStopped {
		cloud.isConnected = true
	}
	cloud.lock.Unlock()

	if isStopped {
		mqttClient.Disconnect(0)
		return
	}

	rpcTopic := fmt.Sprintf("message-router/%s/+/rpc", cloud.cloudConfig.ID)
	if err := cloud.subscribe(mqttClient, rpcTopic, qos, cloud.onRPCMessage); err != nil {
		return
	}
	if err := cloud.subscribe(mqttClient, "message-router/discovery-request", qosDiscovery, cloud.onDiscoveryMessage); err != nil {
		return
	}
}

func (cloud *cloudService) subscribe(mqttClient mqtt.Client, topic string, qos byte, onMessage func(mqtt.Client, mqtt.Message)) error {
	fullTopic := fmt.Sprintf("%s/%s", cloud.cloudConfig.Realm, topic)
	token := mqttClient.Subscribe(fullTopic, qos, onMessage)
	token.Wait()
	if err := token.Error(); err != nil {
		out.Warn("Cannot subscribe to %s: %s", topic, err.Error())
		return err
	}
	return nil
}

func (cloud *cloudService) onLost(_ mqtt.Client, err error) {
	reconnecting := make(chan bool)

	cloud.lock.Lock()
	isStopped := cloud.isStopped
	if !isStopped {
		cloud.isConnected = false
		cloud.reconnecting = reconnecting
	}
	cloud.lock.Unlock()

	if isStopped {
		return
	}

	out.Print("Connection to IoT lost: %s", err.Error())
	go cloud.connectLoop(reconnecting)
}

func (cloud *cloudService) onRPCMessage(_ mqtt.Client, message mqtt.Message) {
	cloud.lock.Lock()
	isStopped := cloud.isStopped
	cloud.lock.Unlock()

	if isStopped {
		return
	}

	topicParts := strings.Split(message.Topic(), "/")
	clientID := topicParts[3]

	cloud.clientsLock.Lock()
	client, clientExists := cloud.clients[clientID]
	if !clientExists {
		client = newClient(clientID, cloud)
		cloud.clients[clientID] = client
	}
	cloud.clientsLock.Unlock()

	if !clientExists {
		out.Print("New Client %s", clientID)
		cloud.ipc.NewCodecClient(client, ioc.ClientTypeIoT)
		go cloud.watchClient(client)
	}

	client.onRPCMessage(message)
}

func (cloud *cloudService) sendRPCMessage(fromID string, payload string) (mqtt.Token, error) {
	topic := fmt.Sprintf("%s/message-router/%s/%s/rpc", cloud.cloudConfig.Realm, fromID, cloud.cloudConfig.ID)

	cloud.lock.Lock()
	isConnected := cloud.isConnected
	client := cloud.client
	cloud.lock.Unlock()

	if !isConnected {
		return nil, fmt.Errorf("MQTT disconnected")
	}

	return client.Publish(topic, qos, false, payload), nil
}

type discoveryResponse struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func (cloud *cloudService) onDiscoveryMessage(client mqtt.Client, _ mqtt.Message) {
	now := time.Now()

	cloud.lock.Lock()
	isStopped := cloud.isStopped
	sendRespone := cloud.lastDiscoveryResponse.Add(discoveryResponseDebounce).Before(now)
	if sendRespone {
		cloud.lastDiscoveryResponse = now
	}
	cloud.lock.Unlock()

	if isStopped || !sendRespone {
		return
	}

	response, _ := json.Marshal(discoveryResponse{
		ID:   cloud.cloudConfig.ID,
		Name: cloud.config.GetConfig().Name,
	})
	topic := fmt.Sprintf("%s/message-router/discovery-response", cloud.cloudConfig.Realm)

	token := client.Publish(topic, qosDiscovery, false, response)
	token.Wait()
	if err := token.Error(); err != nil {
		out.Warn("Cannot publish discovery reply: %s", err.Error())
	}
}

func (cloud *cloudService) cleanUp() {
	stopped := cloud.stopped
	for {
		select {
		case <-stopped:
			return
		case <-cloud.cleanupTicker.C:
			now := time.Now()
			var removedClients []*rpcClient

			cloud.clientsLock.Lock()
			for id, client := range cloud.clients {
				if client.getLastMessageTime().Add(clientInactiveTimeout).Before(now) {
					removedClients = append(removedClients, client)
					delete(cloud.clients, id)
				}
			}
			cloud.clientsLock.Unlock()

			for _, client := range removedClients {
				client.Close()
			}
		}
	}
}

func (cloud *cloudService) watchClient(client *rpcClient) {
	client.waitForClose()

	cloud.clientsLock.Lock()
	if cloud.clients[client.id] == client {
		delete(cloud.clients, client.id)
	}
	cloud.clientsLock.Unlock()
}
