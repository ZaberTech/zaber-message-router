package ipc

import (
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

type localShareService struct {
	localShare ioc.LocalShare
}

func newLocalShareService(localShare ioc.LocalShare) *localShareService {
	service := &localShareService{
		localShare: localShare,
	}
	return service
}

type localShareStatus struct {
	IsRunning                bool   `json:"is_running"`
	LastRunError             string `json:"last_run_error,omitempty"`
	IsDiscoverable           bool   `json:"is_discoverable"`
	LastDiscoverabilityError string `json:"last_discoverability_error,omitempty"`
}

func (service *localShareService) GetStatus() (*localShareStatus, error) {
	isRunning, lastRunError := service.localShare.GetRunningStatus()
	isDiscoverable, lastDiscoverabilityError := service.localShare.GetDiscoverabilityStatus()

	status := &localShareStatus{
		IsRunning:      isRunning,
		IsDiscoverable: isDiscoverable,
	}
	if lastRunError != nil {
		status.LastRunError = lastRunError.Error()
	}
	if lastDiscoverabilityError != nil {
		status.LastDiscoverabilityError = lastDiscoverabilityError.Error()
	}

	return status, nil
}
