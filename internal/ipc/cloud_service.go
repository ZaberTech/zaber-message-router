package ipc

import (
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

type cloudService struct {
	cloud ioc.Cloud
}

func newCloudService(cloud ioc.Cloud) *cloudService {
	service := &cloudService{
		cloud: cloud,
	}
	return service
}

type cloudStatus struct {
	IsConnected bool `json:"is_connected"`
}

func (service *cloudService) GetStatus() (*cloudStatus, error) {
	status := &cloudStatus{
		IsConnected: service.cloud.IsConnected(),
	}
	return status, nil
}
