package ipc

import (
	"context"

	rpc "github.com/ethereum/go-ethereum/rpc"
	baseUtils "gitlab.com/ZaberTech/zaber-go-lib/pkg/utils"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
)

func getClient(client *rpc.Client) *ipcClient {
	return client.Data.(*ipcClient)
}

func getClientFromCtx(ctx context.Context) *ipcClient {
	rpcClient, _ := rpc.ClientFromContext(ctx)
	return getClient(rpcClient)
}

func newClientCallback(clientType ioc.ClientType) func(*rpc.Client) {
	return func(client *rpc.Client) {
		data := &ipcClient{
			rpc:        client,
			version:    baseUtils.Version{Major: 1},
			clientType: clientType,
			closed:     make(chan interface{}),
		}
		client.Data = data

		go func() {
			client.WaitForClose()
			close(data.closed)
		}()
	}
}

type ipcClient struct {
	rpc        *rpc.Client
	version    baseUtils.Version
	clientType ioc.ClientType
	closed     chan interface{}
}

func (client *ipcClient) Version() baseUtils.Version {
	return client.version
}

func (client *ipcClient) ClientType() ioc.ClientType {
	return client.clientType
}

func (client *ipcClient) Closed() chan interface{} {
	return client.closed
}

func (client *ipcClient) notify(method string, args ...interface{}) {
	ctx := context.Background()
	err := client.rpc.Notify(ctx, method, args...)
	if err != nil {
		out.Warn("Failed to notify client %s", err.Error())
		client.rpc.Close()
	}
}
