package ipc

import (
	"context"

	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

// ipcClient doubles as routingClient to simplify the implementation
type routingClient = ipcClient

type routingService struct {
	routing ioc.Routing
	config  ioc.Config
}

func newRoutingService(routing ioc.Routing, config ioc.Config) *routingService {
	service := &routingService{
		routing: routing,
		config:  config,
	}

	return service
}

func (service *routingService) SendMessage(ctx context.Context, forConnectionID string, message string, id uint32) error {
	client := getClientFromCtx(ctx)

	connection, err := service.routing.GetConnection(dto.ConnectionID(forConnectionID))
	if err != nil {
		return err
	}
	connection.SendMessage(client, message, id)
	return nil
}

func (service *routingService) StartMonitoring(ctx context.Context, connectionID string) error {
	client := getClientFromCtx(ctx)

	connection, err := service.routing.GetConnection(dto.ConnectionID(connectionID))
	if err != nil {
		return err
	}
	return connection.StartMonitoring(client)
}

func (service *routingService) StopMonitoring(ctx context.Context, connectionID string) error {
	client := getClientFromCtx(ctx)

	connection, err := service.routing.GetConnection(dto.ConnectionID(connectionID))
	if err != nil {
		return err
	}
	return connection.StopMonitoring(client)
}

func (service *routingService) Warmup(ctx context.Context, connectionID string) error {
	client := getClientFromCtx(ctx)

	connection, err := service.routing.GetConnection(dto.ConnectionID(connectionID))
	if err != nil {
		return err
	}
	return connection.Warmup(client)
}

func (service *routingService) Cooldown(ctx context.Context, connectionID string) error {
	client := getClientFromCtx(ctx)

	connection, err := service.routing.GetConnection(dto.ConnectionID(connectionID))
	if err != nil {
		return err
	}
	return connection.Cooldown(client)
}

func (client *routingClient) SendReplies(fromConnection dto.ConnectionID, messages []string, id uint32) {
	client.notify("routing_reply", fromConnection, messages, id)
}
func (client *routingClient) SendReplyV1(fromConnection dto.ConnectionID, message string, id uint32) {
	client.notify("routing_reply", fromConnection, message, id)
}

func (client *routingClient) SendError(fromConnection dto.ConnectionID, errToBeSent error) {
	client.notify("routing_error", fromConnection, errToBeSent.Error())
}

func (client *routingClient) MonitorMessage(fromConnection dto.ConnectionID, messages []dto.MonitorMessage, id uint32) {
	client.notify("routing_monitorMessage", fromConnection, messages, id)
}

func (service *routingService) ListConnections() ([]*connectionInfo, error) {
	config := service.config.GetConfig()

	infos := make([]*connectionInfo, len(config.Connections))
	for i, connection := range config.Connections {
		infos[i] = connectionToInfo(connection)
	}

	return infos, nil
}
