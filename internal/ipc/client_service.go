package ipc

import (
	"context"
	"fmt"

	"gitlab.com/ZaberTech/zaber-go-lib/pkg/utils"
)

type clientService struct {
}

func newClientService() *clientService {
	service := &clientService{}
	return service
}

func (service *clientService) SetVersion(ctx context.Context, versionStr string) error {
	version, err := utils.ParseVersion(versionStr)
	if err != nil {
		return err
	}
	if version.Major > MaxSupportedClientVersion {
		return fmt.Errorf("Version %s not supported (router version %s)", versionStr, APIVersion)
	}

	client := getClientFromCtx(ctx)
	client.version = version
	return nil
}
