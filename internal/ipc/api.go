package ipc

import (
	"net"

	rpc "github.com/ethereum/go-ethereum/rpc"
	"gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
	"gitlab.izaber.com/software-public/message-router/internal/utils"
)

const APIVersion = "1.2"
const MaxSupportedClientVersion = 2

type ipcService struct {
	apiRouting  *routingService
	apiConfig   *configService
	apiStatus   *statusService
	cloudConfig *cloudService
	localShare  *localShareService
	control     *controlService
	client      *clientService

	localListener net.Listener
	localServer   *rpc.Server
	remoteServer  *rpc.Server
}

func NewIpc(routing ioc.Routing, config ioc.Config, cloud ioc.Cloud, localShare ioc.LocalShare) ioc.Ipc {
	newIpc := &ipcService{
		apiRouting:  newRoutingService(routing, config),
		apiConfig:   newConfigService(routing, config, cloud, localShare),
		apiStatus:   newStatusService(config),
		cloudConfig: newCloudService(cloud),
		localShare:  newLocalShareService(localShare),
		control:     newControlService(),
		client:      newClientService(),
	}
	return newIpc
}

func (ipc *ipcService) registerServices(server *rpc.Server, local bool) error {
	if err := server.RegisterName("routing", ipc.apiRouting); err != nil {
		return err
	}
	if err := server.RegisterName("status", ipc.apiStatus); err != nil {
		return err
	}
	if err := server.RegisterName("client", ipc.client); err != nil {
		return err
	}
	// Before adding a new non-local service, consider effect of duplicated request.
	// Duplicated requests are therothically possible over IoT.

	if local {
		if err := server.RegisterName("config", ipc.apiConfig); err != nil {
			return err
		}
		if err := server.RegisterName("cloud", ipc.cloudConfig); err != nil {
			return err
		}
		if err := server.RegisterName("localshare", ipc.localShare); err != nil {
			return err
		}
		if err := server.RegisterName("control", ipc.control); err != nil {
			return err
		}
	}
	return nil
}

func (ipc *ipcService) Start(errCtx utils.ErrContext, runControl ioc.RunControl) error {
	ipc.control.runControl = runControl

	ipc.localServer = rpc.NewServer()
	if err := ipc.registerServices(ipc.localServer, true); err != nil {
		return err
	}

	ipc.remoteServer = rpc.NewServer()
	if err := ipc.registerServices(ipc.remoteServer, false); err != nil {
		return err
	}

	pipeName, err := communication.GetMessageRouterPipe()
	if err != nil {
		return err
	}
	listener, err := rpc.CreateIPCEndpoint(pipeName)
	if err != nil {
		return err
	}
	ipc.localListener = listener

	go func() {
		if err := ipc.localServer.ServeListener(ipc.localListener, newClientCallback(ioc.ClientTypeLocal)); err != nil {
			errCtx.ReportErr(err)
		}
	}()

	return nil
}

func (ipc *ipcService) Stop() {
	if ipc.localListener != nil {
		if err := ipc.localListener.Close(); err != nil {
			out.Warn("Cannot close IPC: %s", err.Error())
		}
	}

	if ipc.remoteServer != nil {
		ipc.remoteServer.Stop()
	}
	if ipc.localServer != nil {
		ipc.localServer.Stop()
	}
}

func (ipc *ipcService) NewCodecClient(client ioc.CodecIpcClient, clientType ioc.ClientType) {
	codec := rpc.NewFuncCodec(client, client.Encode, client.Decode)
	go ipc.remoteServer.ServeCodec(codec, newClientCallback(clientType))
}

func (ipc *ipcService) NewCodecConnection(conn net.Conn, clientType ioc.ClientType, finished chan interface{}) {
	go func() {
		ipc.remoteServer.ServeCodec(rpc.NewCodec(conn), newClientCallback(clientType))
		close(finished)
	}()
}
