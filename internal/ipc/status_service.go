package ipc

import (
	"context"

	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

type statusService struct {
	config ioc.Config
}

func newStatusService(config ioc.Config) *statusService {
	service := &statusService{
		config: config,
	}
	return service
}

func (service *statusService) Ping(_ context.Context) error {
	return nil
}

func (service *statusService) GetName(_ context.Context) (string, error) {
	return service.config.GetConfig().Name, nil
}

func (service *statusService) GetApiVersion(_ context.Context) (string, error) { // revive:disable-line:var-naming
	return APIVersion, nil
}
