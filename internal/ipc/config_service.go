package ipc

import (
	"sync"

	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

type configService struct {
	routing    ioc.Routing
	config     ioc.Config
	cloud      ioc.Cloud
	localShare ioc.LocalShare
	lock       sync.Mutex
}

func newConfigService(routing ioc.Routing, config ioc.Config, cloud ioc.Cloud, localShare ioc.LocalShare) *configService {
	service := &configService{
		routing:    routing,
		config:     config,
		cloud:      cloud,
		localShare: localShare,
	}
	return service
}

type addSerialRequest struct {
	SerialPort string `json:"serial_port"`
	BaudRate   uint   `json:"baud_rate,omitempty"`
}

func (service *configService) AddSerialPort(request *addSerialRequest) (*connectionInfo, error) {
	service.lock.Lock()
	defer service.lock.Unlock()

	connection, err := service.config.AddSerial(request.SerialPort, request.BaudRate)
	if err != nil {
		return nil, err
	}

	if err := service.routing.AddConnection(connection); err != nil {
		return nil, err
	}
	return connectionToInfo(connection), nil
}

type addTCPIPRequest struct {
	Hostname string `json:"hostname"`
	TCPPort  uint   `json:"tcp_port"`
}

func (service *configService) AddTCPIP(request *addTCPIPRequest) (*connectionInfo, error) {
	service.lock.Lock()
	defer service.lock.Unlock()

	connection, err := service.config.AddTCPIP(request.Hostname, request.TCPPort)
	if err != nil {
		return nil, err
	}

	if err := service.routing.AddConnection(connection); err != nil {
		return nil, err
	}
	return connectionToInfo(connection), nil
}

func (service *configService) RemoveConnection(connectionID string) error {
	service.lock.Lock()
	defer service.lock.Unlock()

	id := dto.ConnectionID(connectionID)

	if err := service.config.RemoveConnection(id); err != nil {
		return err
	}

	return service.routing.RemoveConnection(id)
}

func (service *configService) SetCloudConfig(config *dto.CloudConfig) error {
	service.lock.Lock()
	defer service.lock.Unlock()

	if err := service.config.SetCloudConfig(config); err != nil {
		return err
	}

	service.cloud.ReloadConfig()
	return nil
}

func (service *configService) GetCloudConfig() (*dto.CloudConfig, error) {
	return service.config.GetConfig().Cloud, nil
}

func (service *configService) SetLocalShareConfig(config *dto.LocalShareConfig) error {
	service.lock.Lock()
	defer service.lock.Unlock()

	if err := service.config.SetLocalShareConfig(config); err != nil {
		return err
	}

	service.localShare.ReloadConfig()
	return nil
}

func (service *configService) GetLocalShareConfig() (*dto.LocalShareConfig, error) {
	return service.config.GetConfig().LocalShare, nil
}

type connectionInfo struct {
	Type string `json:"type"`
	ID   string `json:"id"`

	SerialPort string `json:"serial_port,omitempty"`
	BaudRate   uint   `json:"baud_rate,omitempty"`

	Hostname string `json:"hostname,omitempty"`
	TCPPort  uint   `json:"tcp_port,omitempty"`

	Name string `json:"name"`
}

func connectionToInfo(connection *dto.Connection) *connectionInfo {
	info := &connectionInfo{
		Type: connection.Type,
		ID:   string(dto.MakeIdentifier(connection)),
		Name: connection.Name,
	}
	switch connection.Type {
	case dto.ConnectionTypeSerial:
		info.SerialPort = connection.SerialPort
		info.BaudRate = connection.BaudRate
	case dto.ConnectionTypeTCPIP:
		info.Hostname = connection.Hostname
		info.TCPPort = connection.TCPPort
	}
	return info
}

func (service *configService) SetName(name string) error {
	service.lock.Lock()
	defer service.lock.Unlock()

	if err := service.config.SetName(name); err != nil {
		return err
	}

	service.localShare.ReloadName()
	return nil
}

func (service *configService) SetConnectionName(connectionID string, name string) error {
	service.lock.Lock()
	defer service.lock.Unlock()

	id := dto.ConnectionID(connectionID)

	return service.config.SetConnectionName(id, name)
}

func (service *configService) GetName() (string, error) {
	return service.config.GetConfig().Name, nil
}
