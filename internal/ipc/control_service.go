package ipc

import (
	"context"

	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

type controlService struct {
	runControl ioc.RunControl
}

func newControlService() *controlService {
	service := &controlService{}
	return service
}

func (service *controlService) Exit(_ context.Context) error {
	service.runControl.Stop()
	return nil
}
