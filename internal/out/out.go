package out

import (
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.izaber.com/software-public/message-router/internal/utils"
)

type Logger interface {
	Error(v ...interface{}) error
	Warning(v ...interface{}) error
	Info(v ...interface{}) error
}

func init() {
	log.SetOutput(io.Discard)
}

var logger Logger

func SetLogger(newLogger Logger) {
	logger = newLogger
}

func Print(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)

	if l := logger; l != nil {
		_ = l.Info(msg)
	} else {
		fmt.Println(msg)
	}
}

func Warn(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)

	if l := logger; l != nil {
		_ = l.Warning(msg)
	} else {
		fmt.Println("WARNING: " + msg)
	}
}

func Fatal(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)

	if l := logger; l != nil {
		_ = l.Error(msg)
	} else {
		fmt.Fprintln(os.Stderr, msg)
	}

	for _, value := range v {
		if ok, exitCode := utils.IsExitCodeErr(value); ok {
			os.Exit(exitCode)
			return
		}
	}

	os.Exit(1)
}

func Fatale(err error) {
	Fatal("Error: %s", err)
}
