package routing

import (
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

const replyQueueDebounce = 100 * time.Millisecond
const replyBatchSize = 128

type replyQueue struct {
	client          *clientInfo
	lock            sync.Mutex
	waitingMessages []string
	send            chan interface{}
}

func newReplyQueue(client *clientInfo) *replyQueue {
	queue := &replyQueue{
		client: client,
		send:   make(chan interface{}, 1),
	}
	go queue.queueSendLoop()
	return queue
}

func (queue *replyQueue) enqueue(line string) {
	queue.lock.Lock()
	queue.waitingMessages = append(queue.waitingMessages, line)
	queue.lock.Unlock()

	select {
	case queue.send <- nil:
	default:
	}
}

func (queue *replyQueue) shouldWaitForMore() bool {
	queue.lock.Lock()
	defer queue.lock.Unlock()

	wait := true
	if len(queue.waitingMessages) > 0 {
		wait = strings.HasPrefix(queue.waitingMessages[0], "#")
	}

	return wait
}

func (queue *replyQueue) getWaitingMessages() []string {
	select {
	case <-queue.send:
	default:
	}

	queue.lock.Lock()
	messages := queue.waitingMessages
	queue.waitingMessages = nil
	queue.lock.Unlock()

	return messages
}

func (queue *replyQueue) queueSendLoop() {
	client := queue.client
	for {
		select {
		case <-client.removed:
			return
		case <-queue.send:
		}

		waitForMore := queue.shouldWaitForMore()
		if waitForMore {
			select {
			case <-client.removed:
				return
			case <-time.After(replyQueueDebounce):
			}
		}

		messages := queue.getWaitingMessages()
		for len(messages) != 0 {
			var batch []string
			if len(messages) > replyBatchSize {
				batch = messages[:replyBatchSize]
				messages = messages[replyBatchSize:]
			} else {
				batch = messages
				messages = nil
			}

			id := atomic.AddUint32(&client.nextID, 1) - 1
			client.client.SendReplies(client.connection.identifier, batch, id)
		}
	}
}
