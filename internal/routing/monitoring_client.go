package routing

import (
	"sync"
	"sync/atomic"
	"time"

	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	u "gitlab.izaber.com/software-public/message-router/internal/utils"
)

type monitorClientInfo struct {
	client          ioc.RoutingClient
	connection      *connection
	removed         chan interface{}
	nextID          uint32
	messagesLock    sync.Mutex
	waitingMessages []dto.MonitorMessage
	sendMessages    chan interface{}
}

func newMonitorClientInfo(connection *connection, client ioc.RoutingClient) *monitorClientInfo {
	return &monitorClientInfo{
		client:       client,
		connection:   connection,
		removed:      make(chan interface{}),
		sendMessages: make(chan interface{}, 1),
	}
}

func (info *monitorClientInfo) watchClient() {
	select {
	case <-info.removed:
		return
	case <-info.client.Closed():
		info.connection.removeMonitorClient(info.client)
	}
}

/*
The debouncing with credit prevents message to saturate communication during high traffic
but allows for messages to be passed with low latency during low traffic.
*/
const maxDebounceCredit = time.Second
const maxDebounce = 500 * time.Millisecond
const minDebounce = 20 * time.Millisecond
const monitorBatchSize = 128

func (info *monitorClientInfo) sendLoop() {
	credit := maxDebounceCredit
	for {
		waitStart := time.Now()
		select {
		case <-info.removed:
			return
		case <-info.sendMessages:
		}
		waitDuration := time.Since(waitStart)

		// replenish credit by amount of waiting
		credit = u.MinDur(credit+waitDuration, maxDebounceCredit)

		// use credit to reduce latency
		debounce := u.MaxDur(maxDebounce-credit, minDebounce)
		credit -= maxDebounce - debounce

		select {
		case <-info.removed:
			return
		case <-time.After(debounce):
		}

		messages := info.getWaitingMessages()
		for len(messages) != 0 {
			var batch []dto.MonitorMessage
			if len(messages) > monitorBatchSize {
				batch = messages[:monitorBatchSize]
				messages = messages[monitorBatchSize:]
			} else {
				batch = messages
				messages = nil
			}

			id := atomic.AddUint32(&info.nextID, 1) - 1
			info.client.MonitorMessage(info.connection.identifier, batch, id)
		}
	}
}

func (info *monitorClientInfo) sendLine(line string, source string) {
	info.messagesLock.Lock()
	info.waitingMessages = append(info.waitingMessages, dto.MonitorMessage{Line: line, Source: source})
	info.messagesLock.Unlock()

	select {
	case info.sendMessages <- nil:
	default:
	}
}

func (info *monitorClientInfo) getWaitingMessages() []dto.MonitorMessage {
	select {
	case <-info.sendMessages:
	default:
	}

	info.messagesLock.Lock()
	messages := info.waitingMessages
	info.waitingMessages = nil
	info.messagesLock.Unlock()

	return messages
}
