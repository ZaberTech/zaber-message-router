package routing

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
	"gitlab.com/ZaberTech/zaber-go-lib/pkg/utils"
	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
)

type clientInfo struct {
	client          ioc.RoutingClient
	connection      *connection
	removed         chan interface{}
	messageOrdering *utils.MessageOrdering[string]
	nextID          uint32
	replyQueue      *replyQueue
}
type clientList = map[ioc.RoutingClient]*clientInfo

type connection struct {
	isStopped      bool
	routing        *routing
	config         *dto.Connection
	identifier     dto.ConnectionID
	io             communication.IO
	lock           sync.Mutex
	clients        clientList
	idMapping      *idRemapping
	monitorClients monitorClientList
}

type monitorClientList = map[ioc.RoutingClient]*monitorClientInfo

func newConnection(routing *routing, config *dto.Connection) (*connection, error) {
	newConnection := &connection{
		routing:        routing,
		config:         config,
		identifier:     dto.MakeIdentifier(config),
		clients:        make(clientList),
		idMapping:      newIDRemapping(),
		monitorClients: make(monitorClientList),
	}

	return newConnection, nil
}

func (connection *connection) openIO() (communication.IO, error) {
	config := connection.config
	switch config.Type {
	case dto.ConnectionTypeSerial:
		serial, err := communication.OpenSerial(config.SerialPort, (int)(config.BaudRate))
		if err != nil {
			return nil, fmt.Errorf("Cannot open serial port %s: %s", connection.identifier, err.Error())
		}
		return serial, nil
	case dto.ConnectionTypeTCPIP:
		tcp, err := communication.OpenTCP(config.Hostname, (int)(config.TCPPort))
		if err != nil {
			return nil, fmt.Errorf("Cannot open TCP/IP to %s: %s", connection.identifier, err.Error())
		}
		return tcp, nil
	}
	return nil, fmt.Errorf("Unknown connection type %s", config.Type)
}

func (connection *connection) closeIO(io communication.IO) {
	if err := io.Close(); err != nil {
		out.Warn("Error closing %s: %s", connection.identifier, err.Error())
	}
}

func (connection *connection) connectIO() (communication.IO, error) {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	if connection.isStopped {
		return nil, fmt.Errorf("Connection %s is already stopped", connection.identifier)
	}

	isConnected := connection.io != nil
	if !isConnected {
		out.Print("Opening %s", connection.identifier)

		io, err := connection.openIO()
		if err != nil {
			return nil, err
		}

		connection.io = io
		go connection.readLoop(io)
	}

	return connection.io, nil
}

func (connection *connection) stop() {
	connection.lock.Lock()
	connection.isStopped = true
	connection.coolDown()
	connection.lock.Unlock()

	for _, info := range connection.getClients() {
		info.sendError(fmt.Errorf("Connection stopped"))
	}
}

func (connection *connection) readLoop(io communication.IO) {
	for {
		line, err := io.ReadLine()
		if err != nil {
			connection.handleError(err, io)
			return
		}

		isReply, reply, info := connection.idMapping.remapIDBack(line)
		if connection.isInfoValid(info) {
			info.sendLine(reply)
		} else if !isReply {
			for _, info := range connection.getClients() {
				info.sendLine(line)
			}
		}

		for _, monitorClient := range connection.getMonitorClients() {
			var source string
			if isReply && info != nil && info.client == monitorClient.client {
				source = "reply"
			} else if isReply {
				source = "reply-other"
			} else {
				source = "broadcast"
			}
			monitorClient.sendLine(line, source)
		}
	}
}

func (connection *connection) handleError(err error, io communication.IO) {
	connection.lock.Lock()
	isActual := connection.io == io
	if isActual {
		connection.io = nil
	}
	connection.lock.Unlock()

	connection.closeIO(io)

	if isActual {
		for _, info := range connection.getClients() {
			info.sendError(err)
		}
	}

	if _, isErrClosed := err.(*communication.ErrConnectionClosed); !isErrClosed {
		out.Warn("Connection %s disconnected: %s", connection.identifier, err.Error())
	}
}

func (connection *connection) getClients() []*clientInfo {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	if len(connection.clients) == 0 {
		return nil
	}

	clients := make([]*clientInfo, 0, len(connection.clients))
	for _, client := range connection.clients {
		clients = append(clients, client)
	}

	return clients
}

func (connection *connection) isInfoValid(info *clientInfo) bool {
	if info == nil {
		return false
	}

	connection.lock.Lock()
	defer connection.lock.Unlock()

	return connection.clients[info.client] == info
}

func (connection *connection) updateClientList(client ioc.RoutingClient) *clientInfo {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	info, exists := connection.clients[client]
	if exists {
		return info
	}

	useReplyQueue := client.ClientType() == ioc.ClientTypeIoT && client.Version().Major > 1

	info = &clientInfo{
		client:     client,
		connection: connection,
		removed:    make(chan interface{}),
	}
	if useReplyQueue {
		info.replyQueue = newReplyQueue(info)
	}
	info.messageOrdering = utils.NewMessageOrdering[string](10*time.Second, func() {
		info.sendError(fmt.Errorf("Connection was interrupted by missing packet"))
	})
	connection.clients[client] = info

	go info.loop()

	return info
}

func (connection *connection) removeClient(client ioc.RoutingClient) {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	info, found := connection.clients[client]
	if !found {
		return
	}

	delete(connection.clients, client)
	close(info.removed)

	if len(connection.clients) == 0 {
		connection.coolDown()
	}
}

func (connection *connection) SendMessage(fromClient ioc.RoutingClient, message string, id uint32) {
	info := connection.updateClientList(fromClient)

	if err := info.messageOrdering.Write(message, id); err != nil {
		fromClient.SendError(connection.identifier, fmt.Errorf("Connection not ready: %s", err))
	}
}

func (connection *connection) Warmup(client ioc.RoutingClient) error {
	_, err := connection.connectIO()
	if err != nil {
		return err
	}

	info := connection.updateClientList(client)

	atomic.StoreUint32(&info.nextID, 0)
	info.messageOrdering.Reset()

	return nil
}

func (connection *connection) Cooldown(client ioc.RoutingClient) error {
	connection.removeClient(client)
	return nil
}

func (connection *connection) coolDown() {
	io := connection.io
	connection.io = nil

	if io != nil {
		out.Print("Closing %s", connection.identifier)
		connection.closeIO(io)
	}
}

func (connection *connection) StartMonitoring(client ioc.RoutingClient) error {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	info, exists := connection.monitorClients[client]
	if exists {
		atomic.StoreUint32(&info.nextID, 0)
		return nil
	}

	info = newMonitorClientInfo(connection, client)
	connection.monitorClients[client] = info

	go info.watchClient()
	go info.sendLoop()

	return nil
}

func (connection *connection) StopMonitoring(client ioc.RoutingClient) error {
	connection.removeMonitorClient(client)
	return nil
}

func (connection *connection) getMonitorClients() []*monitorClientInfo {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	if len(connection.monitorClients) == 0 {
		return nil
	}

	clients := make([]*monitorClientInfo, 0, len(connection.monitorClients))
	for _, client := range connection.monitorClients {
		clients = append(clients, client)
	}

	return clients
}

func (connection *connection) removeMonitorClient(client ioc.RoutingClient) {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	info, found := connection.monitorClients[client]
	if !found {
		return
	}

	delete(connection.monitorClients, client)
	close(info.removed)
}

func (info *clientInfo) loop() {
	defer info.messageOrdering.Stop()
	connection := info.connection

	for {
		select {
		case <-info.removed:
			return
		case <-info.client.Closed():
			connection.removeClient(info.client)
			return
		case line := <-info.messageOrdering.OrderedData():
			io, err := connection.connectIO()
			if err != nil {
				info.sendError(err)
				continue
			}

			linesToWrite := connection.idMapping.remapID(line, info)
			if len(linesToWrite) == 0 {
				continue
			}

			if err := io.WriteLines(linesToWrite); err != nil {
				info.sendError(err)
				continue
			}

			for _, monitorClient := range connection.getMonitorClients() {
				var source string
				if info.client == monitorClient.client {
					source = "cmd"
				} else {
					source = "cmd-other"
				}
				for _, cmd := range linesToWrite {
					monitorClient.sendLine(cmd, source)
				}
			}
		}
	}
}

func (info *clientInfo) sendLine(line string) {
	if info.replyQueue != nil {
		info.replyQueue.enqueue(line)
	} else {
		id := atomic.AddUint32(&info.nextID, 1) - 1
		if info.client.Version().Major > 1 {
			info.client.SendReplies(info.connection.identifier, []string{line}, id)
		} else {
			info.client.SendReplyV1(info.connection.identifier, line, id)
		}
	}
}

func (info *clientInfo) sendError(errorToSend error) {
	info.client.SendError(info.connection.identifier, errorToSend)
}
