package routing

import (
	"container/heap"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/ZaberTech/zaber-go-lib/pkg/communication"
)

var requestRegexp = regexp.MustCompile(`^\s*(/\s*\d+\s+\d+)\s+(\d+)`)
var responseRegexp = regexp.MustCompile(`^\s*([@#]\s*\d+\s+\d+)\s+(\d+)`)
var checkSumRegexp = regexp.MustCompile(`:(\S+)$`)

const continuationSymbol = `\`

const recordExpireAfterRequest = 30 * time.Second
const recordExpireAfterResponse = 5 * time.Second

type remappingClient = *clientInfo

type mappingRecord struct {
	id int

	client     remappingClient
	originalID int
	commands   []string

	index  int
	expire time.Time
}

type idRemapping struct {
	lock  sync.Mutex
	queue IDQueue

	idMap map[int]*mappingRecord
}

const maxIDExclusive = 100

func newIDRemapping() *idRemapping {
	instance := &idRemapping{
		idMap: make(map[int]*mappingRecord),

		queue: make(IDQueue, 0, maxIDExclusive),
	}

	now := time.Now()
	for id := 0; id < maxIDExclusive; id++ {
		record := &mappingRecord{
			id:         id,
			originalID: -1,
			expire:     now,
		}

		instance.queue.Push(record)
		instance.idMap[record.id] = record
	}
	heap.Init(&instance.queue)

	return instance
}

func (mapping *idRemapping) remapID(message string, client remappingClient) []string {
	match := requestRegexp.FindStringSubmatch(message)
	if match == nil {
		return []string{message}
	}

	newMessage := strings.TrimSpace(message)
	lrcOK, hasLRC := checkLRC(newMessage)
	if !lrcOK {
		return []string{message}
	}

	idToBeMapped, _ := strconv.Atoi(match[2])

	mapping.lock.Lock()
	defer mapping.lock.Unlock()

	record := mapping.findExistingRecordForReuse(client, idToBeMapped)
	if record != nil {
		record.expire = time.Now().Add(recordExpireAfterRequest)
		heap.Fix(&mapping.queue, record.index)
	} else {
		record = heap.Pop(&mapping.queue).(*mappingRecord)
		record.expire = time.Now().Add(recordExpireAfterRequest)
		heap.Push(&mapping.queue, record)

		record.originalID = idToBeMapped
		record.client = client
		record.commands = nil
	}

	newMessage = requestRegexp.ReplaceAllString(newMessage, fmt.Sprintf("$1 %d", record.id))
	if hasLRC {
		newMessage = recomputeLRC(newMessage)
	}

	record.commands = append(record.commands, newMessage)
	if strings.Contains(newMessage, continuationSymbol) {
		return nil
	}
	commands := record.commands
	record.commands = nil
	return commands
}

func (mapping *idRemapping) findExistingRecordForReuse(client remappingClient, idToBeMapped int) *mappingRecord {
	for _, record := range mapping.idMap {
		canBeReused := record.client == client && record.originalID == idToBeMapped
		if canBeReused {
			return record
		}
	}
	return nil
}

func (mapping *idRemapping) remapIDBack(reply string) (isReply bool, newReply string, client remappingClient) {
	match := responseRegexp.FindStringSubmatch(reply)
	if match == nil {
		return false, "", nil
	}

	newReply = strings.TrimSpace(reply)
	lrcOK, hasLRC := checkLRC(newReply)
	if !lrcOK {
		return false, "", nil
	}

	id, _ := strconv.Atoi(match[2])

	var originalID int

	mapping.lock.Lock()

	record, ok := mapping.idMap[id]
	isMapped := ok && record.client != nil
	if isMapped {
		record.expire = time.Now().Add(recordExpireAfterResponse)
		heap.Fix(&mapping.queue, record.index)

		originalID = record.originalID
		client = record.client
	}

	mapping.lock.Unlock()

	if !isMapped {
		return true, "", nil
	}

	newReply = responseRegexp.ReplaceAllString(newReply, fmt.Sprintf("$1 %d", originalID))
	if hasLRC {
		newReply = recomputeLRC(newReply)
	}
	return true, newReply, client
}

func recomputeLRC(message string) string {
	lrc := communication.ComputeLRC(message)
	return checkSumRegexp.ReplaceAllString(message, fmt.Sprintf(":%02X", lrc))
}

func checkLRC(message string) (isOK bool, hasCRC bool) {
	match := checkSumRegexp.FindStringSubmatch(message)
	if match == nil {
		return true, false
	}

	if !communication.VerifyLRC(message, match[1]) {
		return false, true
	}

	return true, true
}
