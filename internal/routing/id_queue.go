package routing

// implementation taken from https://golang.org/pkg/container/heap/#example__priorityQueue

type IDQueue []*mappingRecord

func (queue IDQueue) Len() int { return len(queue) }

func (queue IDQueue) Less(i, j int) bool {
	return queue[i].expire.Before(queue[j].expire)
}

func (queue *IDQueue) Pop() interface{} {
	old := *queue
	n := len(old)
	item := old[n-1]
	item.index = -1
	*queue = old[0 : n-1]
	return item
}

func (queue *IDQueue) Push(x interface{}) {
	n := len(*queue)
	item := x.(*mappingRecord)
	item.index = n
	*queue = append(*queue, item)
}

func (queue IDQueue) Swap(i, j int) {
	queue[i], queue[j] = queue[j], queue[i]
	queue[i].index = i
	queue[j].index = j
}
