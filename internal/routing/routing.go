package routing

import (
	"fmt"
	"sync"

	"gitlab.izaber.com/software-public/message-router/internal/dto"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
)

type routing struct {
	config          ioc.Config
	connections     map[dto.ConnectionID]*connection
	connectionsLock sync.Mutex
}

func NewRouting(config ioc.Config) ioc.Routing {
	newRouting := &routing{
		config:      config,
		connections: make(map[dto.ConnectionID]*connection),
	}
	return newRouting
}

func (routing *routing) Start() error {
	config := routing.config.GetConfig()

	for _, connectionConfig := range config.Connections {
		connection, err := newConnection(routing, connectionConfig)
		if err != nil {
			return err
		}

		routing.connections[connection.identifier] = connection
	}

	return nil
}

func (routing *routing) Stop() {
	for _, connection := range routing.connections {
		connection.stop()
	}
}

func (routing *routing) GetConnection(connectionID dto.ConnectionID) (ioc.RoutingConnection, error) {
	return routing.getConnection(connectionID)
}

func (routing *routing) getConnection(identifier dto.ConnectionID) (*connection, error) {
	routing.connectionsLock.Lock()
	defer routing.connectionsLock.Unlock()

	if connection, ok := routing.connections[identifier]; !ok {
		return nil, fmt.Errorf("Connection with ID %s does not exist", identifier)
	} else {
		return connection, nil
	}
}

func (routing *routing) AddConnection(connectionConfig *dto.Connection) error {
	connection, err := newConnection(routing, connectionConfig)
	if err != nil {
		return err
	}

	routing.connectionsLock.Lock()
	defer routing.connectionsLock.Unlock()

	routing.connections[connection.identifier] = connection

	return nil
}

func (routing *routing) RemoveConnection(connectionID dto.ConnectionID) error {
	routing.connectionsLock.Lock()
	connection, exists := routing.connections[connectionID]
	if exists {
		delete(routing.connections, connectionID)
	}
	routing.connectionsLock.Unlock()

	if !exists {
		return fmt.Errorf("Connection with ID %s does not exist", connectionID)
	}

	connection.stop()
	return nil
}
