package singleinstance

import (
	"errors"
	"os"
	"path/filepath"
	"runtime"

	flock "github.com/theckman/go-flock"
	"gitlab.izaber.com/software-public/message-router/internal/ioc"
	"gitlab.izaber.com/software-public/message-router/internal/out"
	"gitlab.izaber.com/software-public/message-router/internal/utils"
)

type singleInstance struct {
	path string
	lock *flock.Flock
}

func NewSingleInstance() ioc.SingleInstance {
	cacheDir, err := os.UserCacheDir()
	if err != nil {
		cacheDir = "."
	}

	path := filepath.Join(cacheDir, "zaber-message-router.lock")
	return &singleInstance{
		path: path,
		lock: flock.New(path),
	}
}

func (instance *singleInstance) Ensure() error {
	locked, err := instance.lock.TryLock()
	if err != nil {
		out.Warn("Cannot acquire instance lock: %s", err)
	} else if !locked {
		return utils.NewExitCodeError(
			errors.New("Another instance is already running"),
			utils.ExitCodeAnotherInstanceRunning)
	}
	return nil
}

func (instance *singleInstance) Release() {
	isWindows := runtime.GOOS == "windows"
	if !isWindows {
		instance.deleteLockFile()
	}
	if err := instance.lock.Close(); err != nil {
		out.Warn("Cannot release instance lock: %s", err)
	}
	if isWindows {
		instance.deleteLockFile()
	}
}

func (instance *singleInstance) deleteLockFile() {
	if err := os.Remove(instance.path); err != nil {
		out.Warn("Cannot remove instance lock: %s", err)
	}
}
