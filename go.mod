module gitlab.izaber.com/software-public/message-router

go 1.19

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/ethereum/go-ethereum v1.10.4
	github.com/grandcat/zeroconf v1.0.0
	github.com/kardianos/service v1.0.0
	github.com/syohex/go-texttable v0.0.0-20140622065955-d721bde1381e
	github.com/theckman/go-flock v0.8.1
	github.com/zabertech/go-serial v0.0.0-20210201195853-2428148c5139
	gitlab.com/ZaberTech/zaber-go-lib v0.0.0-20221019153758-d8e16982b93e
	golang.org/x/sys v0.10.0
	gopkg.in/urfave/cli.v1 v1.20.0
	gopkg.in/yaml.v2 v2.3.0
)

require (
	github.com/Microsoft/go-winio v0.6.2 // indirect
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/creack/goselect v0.1.2 // indirect
	github.com/deckarep/golang-set v0.0.0-20180603214616-504e848d77ea // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gofrs/flock v0.8.1 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/miekg/dns v1.1.27 // indirect
	golang.org/x/crypto v0.11.0 // indirect
	golang.org/x/net v0.12.0 // indirect
)

replace github.com/ethereum/go-ethereum => github.com/zabertech/go-ethereum v1.10.4-rpc-1
